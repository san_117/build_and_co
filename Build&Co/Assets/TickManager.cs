﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TickManager : MonoBehaviour
{
    [System.Serializable]
    public class TickElement
    {
        public void Tick()
        {

        }
    }
    public int instances = 1000;

    public Queue<TickElement> elements = new Queue<TickElement>();

    private void Start()
    {
        for (int i = 0; i < instances; i++)
        {
            elements.Enqueue(new TickElement());
        }    
    }

    void Update()
    {
        int procecedElementsPerSecond = elements.Count / 60; // List Size / Desired Frames

        for (int i = 0; i <procecedElementsPerSecond; i++)
        {
            TickElement aux = elements.Dequeue();
            aux.Tick();
            elements.Enqueue(aux);
        }
    }
}
