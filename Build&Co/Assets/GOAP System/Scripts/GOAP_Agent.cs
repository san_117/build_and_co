﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GOAP
{
    [RequireComponent(typeof(FSM_Component))]
    public class GOAP_Agent : MonoBehaviour
    {
        public GOAP_Behaviour behaviour;
        private FSM_Component fsm;
        private GOAP_Action currentAction;
        private int current_chain;

        private StateData goal;
        public StateData[] worldStates;
        public Dictionary<string, StateData> worldStates_accesor = new Dictionary<string, StateData>();

        private readonly List<GOAP_Action> aviableActions = new List<GOAP_Action>();
        Astar.ActionChains[] planChains;

        private void Start()
        {
            fsm = GetComponent<FSM_Component>();

            UpdateWorldState();

            CreateActions();
        }

        private void UpdateWorldState()
        {
            worldStates = new StateData[behaviour.worldstates.GetProperties().Length];
            worldStates_accesor.Clear();

            for (int i = 0; i < behaviour.worldstates.GetProperties().Length; i++)
            {
                StateData data = ScriptableObject.CreateInstance(System.Type.GetType(behaviour.worldstates.GetProperty(i).type)) as StateData;
                data.worldState = behaviour.worldstates.GetProperty(i);
                worldStates[i] = data;

                data.Init(this);

                worldStates_accesor.Add(worldStates[i].worldState.tag, worldStates[i]);
            }

            OnUpdateWorldState();
        }

        public virtual void OnUpdateWorldState()
        {

        }

        private void CreateActions()
        {
            foreach (var action in behaviour.actions)
            {
                var action_component = action.actionType;
                GOAP_Action goap_action = gameObject.AddComponent(System.Type.GetType(action.actionType)) as GOAP_Action;
                goap_action.weight = action.weight;

                foreach (var precondition in action.preconditions)
                {
                    StateData data = Instantiate(precondition) as StateData;
                    data.worldState = precondition.worldState;
                    goap_action.preconditions.Add(precondition.UniqueKey(), data);
                }

                foreach (var effect in action.effects)
                {
                    StateData data = Instantiate(effect) as StateData;
                    data.worldState = effect.worldState;
                    goap_action.effects.Add(effect.UniqueKey(), data);
                }

                goap_action.OnAsign(this);

                aviableActions.Add(goap_action);
            }
        }

        public override string ToString()
        {
            if (fsm == null)
                return "Enter in play mode!";

            string output = fsm.ToString() + "\n";
            output += "GOAP Task: ";

            if (currentAction == null)
            {
                output += "Do nothing";
                return output;
            }

            output += currentAction.ToString() + "\n";

            output += "In Queue: " + planChains[current_chain].InQueue();

            return output;
        }

        public void Planning()
        {
            if (goal == null)
                return;

            current_chain = 0;
            planChains = null;

            Dictionary<string, StateData> world = new Dictionary<string, StateData>();

            foreach (var state in worldStates)
            {
                world.Add(state.worldState.tag, state);
            }

            planChains = Astar.GetPlans(goal, world, aviableActions.ToArray());
        }

        /// <summary>
        /// Return true if the local data match to the world;
        /// </summary>
        /// <param name="localData"></param>
        /// <returns></returns>
        public bool CompareLocalDataToWorldData(StateData localData)
        {
            return worldStates_accesor[localData.worldState.tag].Equals(localData);
        }

        public void SetGoal(StateData newGoal)
        {
            this.goal = newGoal;
            currentAction = null;
            Planning();
        }

        private void Update()
        {
            Run();
        }

        public void Run()
        {
            fsm.Run();

            if (planChains != null && planChains.Length > 0)
            {
                if (currentAction == null && planChains[current_chain] != null)
                {
                    Astar.Task task = planChains[current_chain].NextTask();
                    currentAction = task.action;
                    currentAction.SetData(task.data);
                }

                if (currentAction == null)
                {
                    Planning();
                    return;
                }

                switch (currentAction.Perform())
                {
                    case GOAP_Action.Status.PERFORMING:
                        return;
                    case GOAP_Action.Status.COMPLETED:

                        if (planChains[current_chain].InQueue() == 0)
                        {
                            return;
                        }

                        Astar.Task task = planChains[current_chain].NextTask();
                        currentAction = task.action;
                        currentAction.SetData(task.data);
                        return;
                    case GOAP_Action.Status.ABORT:
                        current_chain++;
                        currentAction = null;

                        if (current_chain == planChains.Length)
                        {
                            Planning();
                        }
                        return;
                }
            }
        }
    }

}