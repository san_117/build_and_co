﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GOAP
{
    [CreateAssetMenu(menuName = "AI/GOAP_Behaviour")]
    public class GOAP_Behaviour : ScriptableObject
    {
        public List<ActionNode> actions = new List<ActionNode>();
        public WorldStates worldstates;
        public FSM fsm = new FSM();

        public bool AddAction(ActionNode action)
        {
            if (!actions.Contains(action))
            {
                actions.Add(action);
                return true;
            }

            return false;
        }

        public void RemoveAction(ActionNode action)
        {
            actions.Remove(action);
        }
    }
}