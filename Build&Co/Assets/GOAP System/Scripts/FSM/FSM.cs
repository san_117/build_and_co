﻿using UnityEngine;
using System.Collections.Generic;

namespace GOAP
{
    [System.Serializable]
    public class FSM
    {
        [SerializeField] private List<FSMNode> states = new List<FSMNode>();

        [SerializeField] private string mainNodeID;

        Dictionary<string, FSMNode> nodes_accesor;

        public int Size
        {
            get
            {
                return states.Count;
            }
        }

        public FSMNode GetState(int i)
        {
            return states[i];
        }

        public FSMNode GetState(string uniqueID)
        {
            if (nodes_accesor == null)
            {
                nodes_accesor = new Dictionary<string, FSMNode>();

                foreach (var state in states)
                {
                    nodes_accesor.Add(state.uniqueID, state);
                }
            }

            return nodes_accesor[uniqueID];
        }

        public void SetMainNode(string id)
        {
            mainNodeID = id;
        }

        public string GetMainNodeID()
        {
            return mainNodeID;
        }

        public void Remove(FSMNode node)
        {
            states.Remove(node);

            if (states.Count == 0)
                mainNodeID = string.Empty;
        }

        public FSMNode AddState(string name)
        {
            do
            {
                FSMNode newNode = new FSMNode(name);

                if (!states.Contains(newNode))
                {
                    states.Add(newNode);
                    return newNode;
                }

            } while (true);
        }

        [System.Serializable]
        public class FSMNode
        {
            public string uniqueID;
            public string title;
            public StateData goal;

            public List<FSMConnection> connections = new List<FSMConnection>();

            public FSMNode(string name)
            {
                this.title = name;
                this.uniqueID = Random.Range(0, 9).ToString() + Random.Range(0, 9).ToString() + Random.Range(0, 9).ToString() + Random.Range(0, 9).ToString();
            }

            public override string ToString()
            {
                if (goal == null)
                    return "Seeking what to do...";
                return goal.ToString();
            }

            public override bool Equals(object obj)
            {
                if (obj == null) return false;

                if(obj is FSMNode)
                {
                    return ((obj as FSMNode).uniqueID == uniqueID);
                }

                return false;
            }

            public override int GetHashCode()
            {
                return int.Parse(uniqueID);
            }
        }
    }
}
