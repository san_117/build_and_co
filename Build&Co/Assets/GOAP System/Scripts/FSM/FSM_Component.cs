﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GOAP
{
    [RequireComponent(typeof(GOAP_Agent))]
    public class FSM_Component : MonoBehaviour
    {
        [SerializeField]private FSM brain;
        private GOAP_Agent agent;

        Dictionary<string, FSM.FSMNode> nodes_accesor = new Dictionary<string, FSM.FSMNode>();

        FSM.FSMNode _current;

        private void Start()
        {
            agent = GetComponent<GOAP_Agent>();
            brain = agent.behaviour.fsm;

            if (brain.Size > 0)
            {
                for (int i = 0; i < brain.Size; i++)
                {
                    nodes_accesor.Add(brain.GetState(i).uniqueID, brain.GetState(i));
                }

                SetCurrent(nodes_accesor[brain.GetMainNodeID()]);
            }
            else
            {
                Debug.LogWarning("Fsm need at least one node!");
            }
        }

        private FSM.FSMNode GetCurrent()
        {
            return _current;
        }

        public override string ToString()
        {
            return "FSM State: "+ GetCurrent().title + "\n" + "Current Goal: " + GetCurrent().ToString();
        }

        private void SetCurrent(FSM.FSMNode value)
        {
            if (value != null)
            {
                if (_current == null)
                {
                    _current = value;
                    agent.SetGoal(_current.goal);
                }
                else
                {
                    if (_current != value)
                    {
                        //SET GOAL in GOAP
                        _current = value;
                        agent.SetGoal(_current.goal);
                    }
                }
            }
        }

        public void Run()
        {
            if (GetCurrent() == null)
                return;

            //UPDATE GOAP
            foreach (var connection in GetCurrent().connections)
            {
                foreach (var localdata in connection.conditions)
                {
                    if (!agent.CompareLocalDataToWorldData(localdata))
                        return;
                }

                SetCurrent(brain.GetState(connection.target_NodeID));
                break;
            }
        }
    }
}