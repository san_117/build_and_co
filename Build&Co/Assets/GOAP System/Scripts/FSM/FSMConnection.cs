﻿using System.Collections.Generic;
using UnityEngine;

namespace GOAP
{
    public class FSMConnection: ScriptableObject
    {
        public string target_NodeID;

        public List<StateData> conditions = new List<StateData>();

        public override bool Equals(object obj)
        {
            if (obj == null) return false;

            if (obj is FSMConnection)
            {
                return ((obj as FSMConnection).target_NodeID == target_NodeID);
            }

            return false;
        }

        public override int GetHashCode()
        {
            return int.Parse(target_NodeID);
        }

        public void SetID(string id)
        {
            target_NodeID = id;
        }
    }
}