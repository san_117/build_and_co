﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using UnityEditor.Callbacks;
using System.Linq;
using UnityEditorInternal;

namespace GOAP.Editor
{
    public class GOAP_BehaviourInspector : EditorWindow
    {
        [OnOpenAsset(1)]
        public static bool Open(int instanceID, int line)
        {
            GOAP_Behaviour bt = EditorUtility.InstanceIDToObject(instanceID) as GOAP_Behaviour;
            if (bt != null)
            {
                ShowEditor(bt);
            }

            return false;
        }

        GOAP_Behaviour bt;
        private Vector2 offset;
        private Vector2 drag;

        Vector2 managerPrecondition_scroll;
        Vector2 managerEffects_scroll;
        int worldState_index;
        Rect managerWindow;
        Rect inspectorWindow;

        string[] states_derived;
        string[] states_paths;

        string floatField = "0";

        string[] toolbarOptions = { "GOAP", "Finit State Machine", "Help" };
        int toolbarPage = 0;
        ReorderableList reorderableList;
        FSMConnection selectedConnection;

        Dictionary<string, System.Type> states_types = new Dictionary<string, System.Type>();

        Dictionary<StateData, StateDataInspector> editors = new Dictionary<StateData, StateDataInspector>();

        Dictionary<string, FSMNodeWindow> fsm_windows_accesor = new Dictionary<string, FSMNodeWindow>();

        public List<ActionNodeWindow> actionWindows = new List<ActionNodeWindow>();
        public List<FSMNodeWindow> fsmWindows = new List<FSMNodeWindow>();

        ActionNodeWindow inspectedNode;
        FSMNodeWindow inspectedFSMNode;

        public static void ShowEditor(GOAP_Behaviour bt)
        {
            GOAP_BehaviourInspector editor = GetWindow<GOAP_BehaviourInspector>();
            editor.Init(bt);
        }

        public void Init(GOAP_Behaviour bt)
        {
            foreach (Object o in AssetDatabase.LoadAllAssetsAtPath(AssetDatabase.GetAssetPath(bt)))
            {
                if (o is ActionNodeWindow)
                {
                    ActionNodeWindow action = (o as ActionNodeWindow);

                    var index = 0;
                    foreach (var goapaction in bt.actions)
                    {
                        if (goapaction.actionType == action.action.actionType)
                        {
                            break;
                        }

                        index++;
                    }

                    action.action = bt.actions[index]; 

                    action.OnLoad(RemoveWindow, InspectNode);
                    actionWindows.Add(action);
                    continue;
                }

                if (o is FSMNodeWindow)
                {
                    FSMNodeWindow w = (o as FSMNodeWindow);
                    w.node = bt.fsm.GetState(w.node.uniqueID);

                    fsmWindows.Add(w);
                    fsm_windows_accesor.Add(w.node.uniqueID, w);
                    
                    (o as FSMNodeWindow).OnLoad(bt.fsm, OnInspectFSM, ValidateConnection, RemoveFsmWindow, SetDefaultFSM);
                    continue;
                }

                if (o is StateData)
                {
                    StateData aux = o as StateData;
                    if (!editors.TryGetValue(aux, out StateDataInspector tmpEditor))
                    {
                        tmpEditor = UnityEditor.Editor.CreateEditor(aux) as StateDataInspector;
                        editors.Add(aux, tmpEditor);
                        tmpEditor.Init();
                    }
                    continue;
                }
            }

            this.bt = bt;
            offset = Vector2.zero;
            GUI.backgroundColor = Color.black;

            int e = 0;
            states_derived = new string[System.AppDomain.CurrentDomain.GetAllDerivedTypes(typeof(StateData)).Length];
            foreach (System.Type data in System.AppDomain.CurrentDomain.GetAllDerivedTypes(typeof(StateData)))
            {
                states_derived[e] = data.FullName;
                states_types.Add(states_derived[e], data);
                e++;
            }

            states_paths = new string[states_derived.Length];

            for (int i = 0; i < states_paths.Length; i++)
            {
                states_paths[i] = states_derived[i].Replace('.', '/');
            }
        }

        
        void OnGUI()
        {
            DrawGrid(20, 0.2f, Color.gray);
            DrawGrid(100, 0.4f, Color.gray);

            Event current = Event.current;

            toolbarPage = GUILayout.Toolbar(toolbarPage, toolbarOptions, GUILayout.Height(18));

            BeginWindows();

            managerWindow = new Rect(0, 20, position.width / 5f, position.height - (position.height * 0.15f));
            managerWindow = GUILayout.Window(0, managerWindow, DrawManagerWindow, "World State");

            if (toolbarPage == 0)
            {
                inspectorWindow = new Rect(position.width - (position.width / 4.5f), 20, position.width / 4.6f, position.height - (position.height * 0.1f));
                inspectorWindow = GUILayout.Window(1, inspectorWindow, DrawInspectorWindow, "Inspector", GUILayout.Width(inspectorWindow.width), GUILayout.ExpandWidth(false));

                DrawNodeActionsConnections();

                DrawNodeActions(ref current);

            }else if (toolbarPage == 1)
            {
                inspectorWindow = new Rect(position.width - (position.width / 4.5f), 20, position.width / 4.6f, position.height - (position.height * 0.1f));
                inspectorWindow = GUILayout.Window(1, inspectorWindow, DrawInspectorFSM, "Inspector", GUILayout.Width(inspectorWindow.width), GUILayout.ExpandWidth(false));
                DrawFMSConnections();
                DrawFSMNodes(ref current);
            }

            ProcessEvents(ref current);

            EndWindows();

            if (inspectedFSMNode != null && inspectedFSMNode.startConnecting)
            {
                Repaint();
            }

            if (GUI.changed)
            {
                EditorUtility.SetDirty(bt);
                Repaint();
            }
        }

        private void DrawFSMNodes(ref Event e)
        {
            foreach (var node in fsmWindows)
            {
                node.ProcesssEvents(ref e);
                node.Draw();
            }
        }

        private void DrawFMSConnections()
        {
            foreach (var node in fsmWindows)
            {
                foreach (var conection in node.node.connections)
                {
                    DrawNodeCurve(fsm_windows_accesor[conection.target_NodeID].window,node.window);
                }
            }
        }

        private void DrawNodeActionsConnections()
        {
            foreach (var node in actionWindows)
            {
                foreach (var precondition in node.action.preconditions)
                {
                    foreach (var neighbor in actionWindows)
                    {
                        if (node != neighbor)
                        {
                            foreach (var effect in neighbor.action.effects)
                            {
                                if (effect.Equals(precondition))
                                {
                                    Rect from = node.window;
                                    Rect to = neighbor.window;
                                    DrawNodeCurve(from, to);
                                }
                            }
                        }
                    }
                }
            }
        }

        private void ProcessEvents(ref Event e)
        {
            drag = Vector2.zero;

            switch (e.type)
            {
                case EventType.MouseDown:
                    if (e.button == 1)
                    {
                        ProcessContextMenu(e.mousePosition);
                        e.Use();
                    }
                    break;

                case EventType.MouseDrag:
                    if (e.button == 0)
                    {
                        if (!managerWindow.Contains(e.mousePosition) && !inspectorWindow.Contains(e.mousePosition))
                        {
                            OnDrag(e.delta);
                        }
                    }
                    break;
            }
        }

        private void ProcessContextMenu(Vector2 mousePosition)
        {
            GenericMenu genericMenu = new GenericMenu();

            if (toolbarPage == 0)
            {
                foreach (System.Type action in System.AppDomain.CurrentDomain.GetAllDerivedTypes(typeof(GOAP_Action)))
                {
                    genericMenu.AddItem(new GUIContent(action.Namespace + "/" + action.Name), false, () => AddNodeActionWindow(action.FullName, mousePosition));
                }
            }
            else if (toolbarPage == 1)
            {
                genericMenu.AddItem(new GUIContent("Add State"), false, () => AddNodeFSMWindow(mousePosition));

                if (bt.fsm.Size > 0)
                {
                    genericMenu.AddItem(new GUIContent("Foucus default"), false, () => FocusDefaultState());
                }
            }
            else
            {

            }
            

            genericMenu.ShowAsContext();
        }

        private void FocusDefaultState()
        {
            foreach (var state in fsmWindows)
            {
                state.Focus();
            }
        }

        private void AddNodeFSMWindow(Vector2 mousePos)
        {
            FSM.FSMNode node = bt.fsm.AddState("New State");
            Vector2 pos = mousePos;
            FSMNodeWindow newFSMNodeWindow = CreateInstance<FSMNodeWindow>();
            newFSMNodeWindow.Init(bt.fsm, node ,pos, OnInspectFSM, ValidateConnection, RemoveFsmWindow, SetDefaultFSM);
            newFSMNodeWindow.hideFlags = HideFlags.HideInHierarchy;
            fsm_windows_accesor.Add(node.uniqueID, newFSMNodeWindow);
            fsmWindows.Add(newFSMNodeWindow);

            AssetDatabase.AddObjectToAsset(newFSMNodeWindow, bt);
            AssetDatabase.Refresh();
        }

        private void RemoveFsmWindow(FSMNodeWindow over)
        {
            FreeFSMWindow(over);

            if (inspectedFSMNode == over)
                inspectedFSMNode = null;

            bt.fsm.Remove(over.node);
            fsmWindows.Remove(over);
            AssetDatabase.RemoveObjectFromAsset(over);

            if(over.node.uniqueID == bt.fsm.GetMainNodeID())
            {
                if(bt.fsm.Size > 0)
                {
                    bt.fsm.SetMainNode(bt.fsm.GetState(0).uniqueID);

                    foreach (var window in fsmWindows)
                    {
                        window.Deselect();
                    }
                }
            }
        }

        private void AddNodeActionWindow(string name, Vector2 mousePos)
        {
            ActionNode node = new ActionNode(name);

            if (bt.AddAction(node))
            {
                Vector2 pos = mousePos;
                ActionNodeWindow newAction = CreateInstance<ActionNodeWindow>();
                newAction.Init(node, pos, RemoveWindow, InspectNode);
                newAction.hideFlags = HideFlags.HideInHierarchy;

                actionWindows.Add(newAction);

                AssetDatabase.AddObjectToAsset(newAction, bt);
                AssetDatabase.Refresh();
            }
            else
            {
                foreach (var window in actionWindows)
                {
                    if (window.action.Equals(node))
                    {
                        window.window.center = mousePos;
                        break;
                    }
                }
            }
        }

        private void RemoveWindow(ActionNodeWindow over)
        {
            FreeNodeWindow(over);

            if (inspectedNode.Equals(over))
                inspectedNode = null;

            bt.actions.Remove(over.action);
            actionWindows.Remove(over);
            AssetDatabase.RemoveObjectFromAsset(over);
        }

        private void OnInspectFSM(FSMNodeWindow over)
        {
            reorderableList = new ReorderableList(over.node.connections, typeof(FSMConnection), true, true, false, true);
            inspectedFSMNode = over;
            selectedConnection = null;
        }

        private void InspectNode(ActionNodeWindow over)
        {
            inspectedNode = over;
        }

        private void DrawNodeActions(ref Event e)
        {
            foreach (var window in actionWindows)
            {
                window.ProcesssEvents(ref e);
                window.Draw();
            }
        }

        void DrawInspectorFSM(int id)
        {
            void CreatePreconditionData(object index)
            {
                int i = (int)index;
                StateData data = CreateInstance(states_types[bt.worldstates.GetProperty(i).type]) as StateData;
                data.name = data.GetHashCode().ToString() + ".dat";
                data.worldState = bt.worldstates.GetProperty(i);
                data.hideFlags = HideFlags.HideInHierarchy;
                selectedConnection.conditions.Add(data);
                AssetDatabase.AddObjectToAsset(data, bt);
                AssetDatabase.Refresh();
                EditorUtility.SetDirty(bt);
            }

            void CreateGoalData(object index)
            {
                if(inspectedFSMNode.node.goal != null)
                {
                    editors.Remove(inspectedFSMNode.node.goal);
                    AssetDatabase.RemoveObjectFromAsset(inspectedFSMNode.node.goal);
                }

                int i = (int)index;
                StateData data = CreateInstance(states_types[bt.worldstates.GetProperty(i).type]) as StateData;
                data.name = data.GetHashCode().ToString() + ".dat";
                data.worldState = bt.worldstates.GetProperty(i);
                data.hideFlags = HideFlags.HideInHierarchy;
                inspectedFSMNode.node.goal = data;
                AssetDatabase.AddObjectToAsset(data, bt);
                AssetDatabase.Refresh();
                EditorUtility.SetDirty(bt);
            }

            GUILayout.BeginVertical("General", "window", GUILayout.MinHeight(inspectorWindow.height * 0.1f));

            if (inspectedFSMNode != null)
            {
                GUILayout.BeginHorizontal();
                inspectedFSMNode.node.title = GUILayout.TextField(inspectedFSMNode.node.title);
                GUILayout.EndHorizontal();

                reorderableList.drawElementCallback = (Rect rect, int index, bool active, bool focused) => {
                    EditorGUI.LabelField(rect, fsm_windows_accesor[inspectedFSMNode.node.connections[index].target_NodeID].node.title);
                };
                reorderableList.drawHeaderCallback = (Rect rect) => {
                    EditorGUI.LabelField(rect, "Connection priority");
                };

                reorderableList.onSelectCallback = (ReorderableList list) => {
                    selectedConnection = inspectedFSMNode.node.connections[list.index];
                };

                reorderableList.onRemoveCallback = (ReorderableList l) => {

                    foreach (var stateData in inspectedFSMNode.node.connections[l.index].conditions)
                    {
                        AssetDatabase.RemoveObjectFromAsset(stateData);
                    }

                    AssetDatabase.RemoveObjectFromAsset(inspectedFSMNode.node.connections[l.index]);
                    EditorUtility.SetDirty(bt);
                    ReorderableList.defaultBehaviours.DoRemoveButton(l);
                };

                reorderableList.DoLayoutList();
                GUILayout.BeginVertical("Goal", "window", GUILayout.MinHeight(inspectorWindow.height * 0.25f));

                if (inspectedFSMNode.node.goal == null)
                {
                    if (bt.worldstates.Size > 0)
                    {
                        if (GUILayout.Button("Set Goal"))
                        {
                            GenericMenu genericMenu = new GenericMenu();

                            for (int i = 0; i < bt.worldstates.GetProperties().Length; i++)
                            {
                                genericMenu.AddItem(new GUIContent(bt.worldstates.GetProperties()[i]), false, CreateGoalData, i);
                            }

                            genericMenu.ShowAsContext();
                        }
                    }
                    else
                    {
                        GUILayout.Label("You must have at least one world state!");
                    }

                }
                else
                {
                    if (!editors.TryGetValue(inspectedFSMNode.node.goal, out StateDataInspector tmpEditor))
                    {
                        tmpEditor = UnityEditor.Editor.CreateEditor(inspectedFSMNode.node.goal) as StateDataInspector;
                        editors.Add(inspectedFSMNode.node.goal, tmpEditor);
                        tmpEditor.Init();
                    }

                    tmpEditor.Draw();

                    if (GUILayout.Button("Set Goal"))
                    {
                        GenericMenu genericMenu = new GenericMenu();

                        for (int i = 0; i < bt.worldstates.GetProperties().Length; i++)
                        {
                            genericMenu.AddItem(new GUIContent(bt.worldstates.GetProperties()[i]), false, CreateGoalData, i);
                        }

                        genericMenu.ShowAsContext();
                    }
                }

                GUILayout.EndVertical();
                GUILayout.BeginVertical("Transitions", "window", GUILayout.MinHeight(inspectorWindow.height * 0.6f));

                if (selectedConnection != null)
                {
                    foreach (var condition in selectedConnection.conditions)
                    {
                        GUILayout.BeginVertical(condition.worldState.tag, "window", GUILayout.MinHeight(10));

                        if (!editors.TryGetValue(condition, out StateDataInspector tmpEditor))
                        {
                            tmpEditor = UnityEditor.Editor.CreateEditor(condition) as StateDataInspector;
                            editors.Add(condition, tmpEditor);
                            tmpEditor.Init();
                        }

                        tmpEditor.Draw();

                        if (GUILayout.Button("-"))
                        {
                            editors.Remove(condition);
                            selectedConnection.conditions.Remove(condition);
                            AssetDatabase.RemoveObjectFromAsset(condition);
                            return;
                        }
                        GUILayout.EndVertical();
                    }

                    var buttonlaber = "";

                    if (selectedConnection.conditions.Count > 0)
                    {
                        buttonlaber = "And";
                    }
                    else
                    {
                        buttonlaber = "Add";
                    }
                     
                    if (GUILayout.Button(buttonlaber))
                    {
                        GenericMenu genericMenu = new GenericMenu();

                        for (int i = 0; i < bt.worldstates.GetProperties().Length; i++)
                        {
                            genericMenu.AddItem(new GUIContent(bt.worldstates.GetProperties()[i]), false, CreatePreconditionData, i);
                        }

                        genericMenu.ShowAsContext();
                    }
                }
                else
                {
                    GUILayout.Label("Select a connection");
                }

                
                GUILayout.EndVertical();
            }

            GUILayout.EndVertical();
        }

        void SetDefaultFSM(FSMNodeWindow over)
        {
            foreach (var window in fsmWindows)
            {
                window.Deselect();
            }

            over.Select();
        }

        void ValidateConnection(FSMNodeWindow over)
        {
            foreach (var fsmNode in fsmWindows)
            {
                if (!over.Equals(fsmNode))
                {
                    if (fsmNode.window.Contains(Event.current.mousePosition))
                    {
                        FSMConnection connection = CreateInstance<FSMConnection>();

                        over.node.connections.Add(connection);

                        connection.SetID(fsmNode.node.uniqueID);
                        connection.name = connection.GetHashCode().ToString() + ".dat";
                        connection.hideFlags = HideFlags.HideInHierarchy;
                        AssetDatabase.AddObjectToAsset(connection, over);
                        EditorUtility.SetDirty(bt);
                        AssetDatabase.Refresh();
                        over.Select();
                        break;
                    }
                }
            }
        }

        void DrawInspectorWindow(int id)
        {
            void CreatePreconditionData(object index)
            {
                int i = (int)index;
                StateData data = CreateInstance(states_types[bt.worldstates.GetProperty(i).type]) as StateData;
                data.name = data.GetHashCode().ToString() + ".dat";
                data.worldState = bt.worldstates.GetProperty(i);
                data.hideFlags = HideFlags.HideInHierarchy;
                inspectedNode.action.preconditions.Add(data);
                AssetDatabase.AddObjectToAsset(data, bt);
                AssetDatabase.Refresh();
                EditorUtility.SetDirty(bt);
            }

            void CreateEffectData(object index)
            {
                int i = (int)index;
                StateData data = CreateInstance(states_types[bt.worldstates.GetProperty(i).type]) as StateData;
                data.name = data.GetHashCode().ToString() + ".dat";
                data.worldState = bt.worldstates.GetProperty(i);
                data.hideFlags = HideFlags.HideInHierarchy;
                inspectedNode.action.effects.Add(data);
                AssetDatabase.AddObjectToAsset(data, bt);
                AssetDatabase.Refresh();
                EditorUtility.SetDirty(bt);
            }

            GUILayout.BeginVertical();

            if (inspectedNode != null)
            {
                GUILayout.BeginVertical("General", "window", GUILayout.MinHeight(inspectorWindow.height*0.1f));
                GUILayout.Label(inspectedNode.title);
                GUILayout.BeginHorizontal();
                GUILayout.Label("Weight");

                floatField = inspectedNode.action.weight.ToString();

                floatField = GUILayout.TextArea(floatField,3);

                if(float.TryParse(floatField, out float result))
                {
                    inspectedNode.action.weight = result;
                }

                GUILayout.EndHorizontal();
                GUILayout.EndVertical();

                GUILayout.BeginVertical("Preconditions", "window", GUILayout.Height(inspectorWindow.height * 0.45f));
                inspectedNode.scroll_precondition = GUILayout.BeginScrollView(inspectedNode.scroll_precondition);

                for (int i = 0; i < inspectedNode.action.preconditions.Count; i++)
                {
                    GUILayout.BeginVertical(inspectedNode.action.preconditions[i].worldState.tag, "window", GUILayout.MinHeight(10));

                    if (!editors.TryGetValue(inspectedNode.action.preconditions[i], out StateDataInspector tmpEditor))
                    {
                        tmpEditor = UnityEditor.Editor.CreateEditor(inspectedNode.action.preconditions[i]) as StateDataInspector;
                        editors.Add(inspectedNode.action.preconditions[i], tmpEditor);
                        tmpEditor.Init();
                    }

                    tmpEditor.Draw();

                    if (GUILayout.Button("-"))
                    {
                        AssetDatabase.RemoveObjectFromAsset(inspectedNode.action.preconditions[i]);
                        inspectedNode.action.preconditions.Remove(inspectedNode.action.preconditions[i]);
                        return;
                    }

                    EditorGUILayout.EndVertical();
                }

                EditorGUILayout.EndScrollView();

                if (bt.worldstates.Size > 0)
                {
                    if (GUILayout.Button("+"))
                    {
                        GenericMenu genericMenu = new GenericMenu();

                        for (int i = 0; i < bt.worldstates.GetProperties().Length; i++)
                        {
                            genericMenu.AddItem(new GUIContent(bt.worldstates.GetProperties()[i]) ,false ,CreatePreconditionData, i);
                        }

                        genericMenu.ShowAsContext();
                    }
                }
                GUILayout.EndVertical();

                GUILayout.BeginVertical("Effects", "window", GUILayout.Height(inspectorWindow.height * 0.45f));
                inspectedNode.scroll_effect = GUILayout.BeginScrollView(inspectedNode.scroll_effect);

                for (int i = 0; i < inspectedNode.action.effects.Count; i++)
                {
                    GUILayout.BeginVertical(inspectedNode.action.effects[i].worldState.tag, "window", GUILayout.MinHeight(10));

                    if (!editors.TryGetValue(inspectedNode.action.effects[i], out StateDataInspector tmpEditor))
                    {
                        tmpEditor = UnityEditor.Editor.CreateEditor(inspectedNode.action.effects[i]) as StateDataInspector;
                        editors.Add(inspectedNode.action.effects[i], tmpEditor);
                        tmpEditor.Init();
                    }

                    tmpEditor.Draw();

                    if (GUILayout.Button("-"))
                    {
                        AssetDatabase.RemoveObjectFromAsset(inspectedNode.action.effects[i]);
                        inspectedNode.action.effects.Remove(inspectedNode.action.effects[i]);
                        return;
                    }

                    EditorGUILayout.EndVertical();
                }

                EditorGUILayout.EndScrollView();

                if (bt.worldstates.Size > 0)
                {
                    if (GUILayout.Button("+"))
                    {
                        GenericMenu genericMenu = new GenericMenu();

                        for (int i = 0; i < bt.worldstates.GetProperties().Length; i++)
                        {
                            genericMenu.AddItem(new GUIContent(bt.worldstates.GetProperties()[i]), false, CreateEffectData, i);
                        }

                        genericMenu.ShowAsContext();
                    }
                }
                GUILayout.EndVertical();
            }
            else
            {
                GUILayout.Label("Select a node");
            }

            GUILayout.EndVertical();
        }

        void FreeFSMWindow(FSMNodeWindow window)
        {
            foreach (var node in fsmWindows)
            {
                if (node.node.uniqueID != window.node.uniqueID)
                {
                    List<FSMConnection> toDelete = new List<FSMConnection>();

                    foreach (var connection in node.node.connections)
                    {
                        if (connection.target_NodeID == window.node.uniqueID)
                        {
                            toDelete.Add(connection);
                        }
                    }

                    for (int i = toDelete.Count - 1; i >= 0; i--)
                    {

                        foreach (var stateData in toDelete[i].conditions)
                        {
                            editors.Remove(stateData);
                            AssetDatabase.RemoveObjectFromAsset(stateData);
                        }

                        node.node.connections.Remove(toDelete[i]);
                        AssetDatabase.RemoveObjectFromAsset(toDelete[i]);
                    }
                    editors.Remove(window.node.goal);
                    AssetDatabase.RemoveObjectFromAsset(window.node.goal);
                }
            }

            foreach (var connection in window.node.connections)
            {
                foreach (var stateData in connection.conditions)
                {
                    editors.Remove(stateData);
                    AssetDatabase.RemoveObjectFromAsset(stateData);
                }

                AssetDatabase.RemoveObjectFromAsset(connection);
            }
        }

        void FreeNodeWindow(ActionNodeWindow window)
        {
            List<StateData> toErase = new List<StateData>();
            foreach (StateData precondition in window.action.preconditions)
            {
                toErase.Add(precondition);
            }

            for (int e = toErase.Count() - 1; e >= 0; e--)
            {
                AssetDatabase.RemoveObjectFromAsset(toErase[e]);
                editors.Remove(toErase[e]);
                window.action.preconditions.Remove(toErase[e]);
            }

            toErase.Clear();

            foreach (StateData effect in window.action.effects)
            {
                toErase.Add(effect);
            }

            for (int e = toErase.Count() - 1; e >= 0; e--)
            {
                AssetDatabase.RemoveObjectFromAsset(toErase[e]);
                editors.Remove(toErase[e]);
                window.action.effects.Remove(toErase[e]);
            }
        }

        void DrawManagerWindow(int id)
        {
            GUILayout.BeginVertical(GUILayout.Width(managerWindow.width), GUILayout.Height(managerWindow.height));
            managerPrecondition_scroll = GUILayout.BeginScrollView(managerPrecondition_scroll, GUILayout.Height(managerWindow.height));

            for (int i = 0; i < bt.worldstates.Size; i++)
            {
                GUILayout.BeginVertical();
                GUILayout.BeginHorizontal();
                bt.worldstates.GetProperty(i).tag = GUILayout.TextField(bt.worldstates.GetProperty(i).tag, GUILayout.Width(1.2f * managerWindow.width / 3));
                GUILayout.Label(states_types[bt.worldstates.GetProperty(i).type].Name, GUILayout.Width(80));

                if (GUILayout.Button("-", GUILayout.Width(20), GUILayout.Height(20)))
                {
                    foreach (var fsmState in fsmWindows)
                    {
                        List<StateData> toErase = new List<StateData>();
                        if (fsmState.node.goal.worldState.tag == bt.worldstates.GetProperty(i).tag)
                        {
                            toErase.Add(fsmState.node.goal);
                        }

                        for (int e = toErase.Count() - 1; e >= 0; e--)
                        {
                            AssetDatabase.RemoveObjectFromAsset(toErase[e]);
                            editors.Remove(toErase[e]);
                            fsmState.node.goal = null; 
                        }
                    }

                    foreach (var nodeWindow in actionWindows)
                    {
                        List<StateData> toErase = new List<StateData>();
                        foreach (StateData precondition in nodeWindow.action.preconditions)
                        {
                            if (precondition.worldState.tag == bt.worldstates.GetProperty(i).tag)
                            {
                                toErase.Add(precondition);
                            }
                        }

                        for (int e = toErase.Count() - 1; e >= 0; e--)
                        {
                            AssetDatabase.RemoveObjectFromAsset(toErase[e]);
                            editors.Remove(toErase[e]);
                            nodeWindow.action.preconditions.Remove(toErase[e]);
                        }

                        toErase.Clear();

                        foreach (StateData effect in nodeWindow.action.effects)
                        {
                            if (effect.worldState.tag == bt.worldstates.GetProperty(i).tag)
                            {
                                toErase.Add(effect);
                            }
                        }

                        for (int e = toErase.Count() - 1; e >= 0; e--)
                        {
                            AssetDatabase.RemoveObjectFromAsset(toErase[e]);
                            editors.Remove(toErase[e]);
                            nodeWindow.action.effects.Remove(toErase[e]);
                        }
                    }

                    bt.worldstates.RemovePropertyAt(i);
                    return;
                }

                GUILayout.EndHorizontal();
                GUILayout.EndVertical();
            }

            EditorGUILayout.EndScrollView();

            GUILayout.BeginHorizontal();

            worldState_index = EditorGUILayout.Popup(worldState_index, states_paths);

            if (GUILayout.Button("Add WorldState"))
            {
                if (!bt.worldstates.AddPorperty("worldState", states_derived[worldState_index]))
                {
                    bt.worldstates.AddPorperty("worldState " + bt.worldstates.Size, states_derived[worldState_index]);
                }
            }

            GUILayout.EndHorizontal();
            GUILayout.EndVertical();
            GUI.BringWindowToFront(id);
        }

        void DrawNodeCurve(Rect start, Rect end)
        {
            Vector3 startPos = new Vector3(start.x+5, start.y + start.height / 2, 0);
            Vector3 endPos = new Vector3(end.x + end.width - 5, end.y + end.height / 2, 0);
            Vector3 startTan = startPos - Vector3.right * 50;
            Vector3 endTan = endPos - Vector3.left * 50;
            Color shadowCol = new Color(0, 0, 0, 0.06f);
            for (int i = 0; i < 3; i++) // Draw a shadow
                Handles.DrawBezier(startPos, endPos, startTan, endTan, shadowCol, null, (i + 1) * 5);
            Handles.DrawBezier(startPos, endPos, startTan, endTan, Color.black, null, 1);
        }

        private void OnDrag(Vector2 delta)
        {
            drag = delta;

            if (toolbarPage == 0)
            {
                foreach (var actionWindow in actionWindows)
                {
                    actionWindow.Drag(delta);
                }
            }
            else if(toolbarPage == 1)
            {
                foreach (var fsm in fsmWindows)
                {
                    fsm.Drag(delta);
                }
            }

            GUI.changed = true;
        }

        private void DrawGrid(float gridSpacing, float gridOpacity, Color gridColor)
        {
            int widthDivs = Mathf.CeilToInt(position.width / gridSpacing);
            int heightDivs = Mathf.CeilToInt(position.height / gridSpacing);

            Handles.BeginGUI();
            Handles.color = new Color(gridColor.r, gridColor.g, gridColor.b, gridOpacity);

            offset += drag * 0.5f;
            Vector3 newOffset = new Vector3(offset.x % gridSpacing, offset.y % gridSpacing, 0);

            for (int i = 0; i < widthDivs; i++)
            {
                Handles.DrawLine(new Vector3(gridSpacing * i, -gridSpacing, 0) + newOffset, new Vector3(gridSpacing * i, position.height, 0f) + newOffset);
            }

            for (int j = 0; j < heightDivs; j++)
            {
                Handles.DrawLine(new Vector3(-gridSpacing, gridSpacing * j, 0) + newOffset, new Vector3(position.width, gridSpacing * j, 0f) + newOffset);
            }

            Handles.color = Color.black;
            Handles.EndGUI();
        }
    }
}