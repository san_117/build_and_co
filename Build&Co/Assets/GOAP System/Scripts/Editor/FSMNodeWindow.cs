﻿using UnityEngine;
using UnityEditor;
using System;

namespace GOAP.Editor
{
    public class FSMNodeWindow : ScriptableObject
    {
        public FSM fsm;
        public FSM.FSMNode node;

        public Rect window;
        [SerializeField] float dragged_x, dragged_y;
        private bool isDragged;

        public GUIStyle currentStyle;
        Texture2D defaultStyle;
        Texture2D mainStyle;
        Texture2D default_selected_Style;
        Texture2D main_selected_Style;

        Action<FSMNodeWindow> onSelect;
        Action<FSMNodeWindow> onFinishConnection;
        Action<FSMNodeWindow> onMainSet;
        Action<FSMNodeWindow> onRemove;

        public bool startConnecting;

        public override bool Equals(object other)
        {
            if (other == null)
                return false;

            if (other is FSMNodeWindow)
            {
                return (node.uniqueID == (other as FSMNodeWindow).node.uniqueID);
            }

            return false;
        }

        public override int GetHashCode()
        {
            return node.GetHashCode();
        }

        public void Init(FSM fsm, FSM.FSMNode node ,Vector2 pos, Action<FSMNodeWindow> onSelect, Action<FSMNodeWindow> onFinishConnection, Action<FSMNodeWindow> onRemove, Action<FSMNodeWindow> onMainSet)
        {
            this.fsm = fsm;
            this.node = node;
            this.onSelect = onSelect;
            this.onFinishConnection = onFinishConnection;
            this.onRemove = onRemove;
            this.onMainSet = onMainSet;

            window = new Rect(pos.x, pos.y, 200, 50);

            currentStyle = new GUIStyle();
            currentStyle.alignment = TextAnchor.MiddleCenter;
            currentStyle.border = new RectOffset(12, 12, 12, 12);

            defaultStyle = EditorGUIUtility.Load("builtin skins/darkskin/images/node1.png") as Texture2D;
            mainStyle = EditorGUIUtility.Load("builtin skins/darkskin/images/node2.png") as Texture2D;
            default_selected_Style = EditorGUIUtility.Load("builtin skins/darkskin/images/node1 on.png") as Texture2D;
            main_selected_Style = EditorGUIUtility.Load("builtin skins/darkskin/images/node2 on.png") as Texture2D;

            if (fsm.Size == 1)
            {
                fsm.SetMainNode(node.uniqueID);
            }

            if (fsm.GetMainNodeID() == node.uniqueID)
            {
                currentStyle.normal.background = mainStyle;
            }
            else
            {
                currentStyle.normal.background = defaultStyle;
            }
        }

        public void OnLoad(FSM fsm, Action<FSMNodeWindow> onSelect, Action<FSMNodeWindow> onFinishConnection, Action<FSMNodeWindow> onRemove, Action<FSMNodeWindow> onMainSet)
        {
            this.fsm = fsm;
            this.onSelect = onSelect;
            this.onFinishConnection = onFinishConnection;
            this.onRemove = onRemove;
            this.onMainSet = onMainSet;

            defaultStyle = EditorGUIUtility.Load("builtin skins/darkskin/images/node1.png") as Texture2D;
            mainStyle = EditorGUIUtility.Load("builtin skins/darkskin/images/node2.png") as Texture2D;
            default_selected_Style = EditorGUIUtility.Load("builtin skins/darkskin/images/node1 on.png") as Texture2D;
            main_selected_Style = EditorGUIUtility.Load("builtin skins/darkskin/images/node2 on.png") as Texture2D;
        }

        public void Draw()
        {
            Event e = Event.current;

            if (startConnecting)
            {
                Vector2 pos = e.mousePosition;
                DrawNodeCurve(window, new Rect(pos.x, pos.y, window.width, window.height));
            }

            GUIContent content = new GUIContent(node.title);

            GUI.Box(window, content, currentStyle);
        }

        public void Select()
        {
            if (fsm.GetMainNodeID() == node.uniqueID)
            {
                currentStyle.normal.background = main_selected_Style;
            }
            else
            {
                currentStyle.normal.background = default_selected_Style;
            }

            onSelect.Invoke(this);
        }

        public void Deselect()
        {
            if (fsm.GetMainNodeID() == node.uniqueID)
            {
                currentStyle.normal.background = mainStyle;
            }
            else
            {
                currentStyle.normal.background = defaultStyle;
            }
        }

        public void ProcesssEvents(ref Event e)
        {
            switch (e.type)
            {
                case EventType.MouseDown:

                    if (e.button == 1 && window.Contains(e.mousePosition))
                    {
                        ProcessContextMenu();
                        e.Use();
                    }

                    if (e.button == 0)
                    {
                        if (startConnecting)
                        {
                            onFinishConnection.Invoke(this);
                        }

                        if (window.Contains(e.mousePosition))
                        {
                            isDragged = true;

                            Select();
                        }
                        else
                        {
                            Deselect();
                        }

                        startConnecting = false;

                        GUI.changed = true;
                    }
                    break;

                case EventType.MouseUp:
                    isDragged = false;

                    break;

                case EventType.MouseDrag:
                    if (e.button == 0 && isDragged)
                    {
                        Drag(e.delta);
                        e.Use();
                    }
                    break;
            }
        }

        private void ProcessContextMenu()
        {
            GenericMenu genericMenu = new GenericMenu();

            genericMenu.AddItem(new GUIContent("Make Default"), false, MakeDefault);
            genericMenu.AddItem(new GUIContent("Connect"), false, StartConnection);
            genericMenu.AddItem(new GUIContent("Remove"), false, OnClickRemoveNode);

            genericMenu.ShowAsContext();
        }

        private void StartConnection()
        {
            startConnecting = true;
        }

        private void OnClickRemoveNode()
        {
            onRemove?.Invoke(this);
            GUI.changed = true;
        }

        private void MakeDefault()
        {
            fsm.SetMainNode(node.uniqueID);
            onMainSet?.Invoke(this);
        }

        public void Focus()
        {
            Vector2 delta = new Vector2(dragged_x, dragged_y);
            window.position -= delta;
            dragged_x = 0;
            dragged_y = 0;
        }

        public void Drag(Vector2 delta)
        {
            window.position += delta;
            dragged_x += delta.x;
            dragged_y += delta.y;
            GUI.changed = true;
        }

        void DrawNodeCurve(Rect start, Rect end)
        {
            Vector3 startPos = new Vector3(start.x+start.width/2, start.y + start.height / 2, 0);
            Vector3 endPos = new Vector3(end.x, end.y, 0);
            Vector3 startTan = startPos - Vector3.right * 50;
            Vector3 endTan = endPos - Vector3.left * 50;
            Color shadowCol = new Color(0, 0, 0, 0.06f);
            for (int i = 0; i < 3; i++) // Draw a shadow
                Handles.DrawBezier(startPos, endPos, startTan, endTan, shadowCol, null, (i + 1) * 5);
            Handles.DrawBezier(startPos, endPos, startTan, endTan, Color.black, null, 1);
        }
    }
}
