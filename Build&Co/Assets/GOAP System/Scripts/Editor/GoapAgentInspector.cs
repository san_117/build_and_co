﻿
using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

namespace GOAP.Editor
{
    [CustomEditor(typeof(GOAP_Agent), true)]
    public class GoapAgentInspector : UnityEditor.Editor
    {
        GOAP_Agent agent;
        public Dictionary<string, StateDataInspector> editors = new Dictionary<string, StateDataInspector>();

        private void OnEnable()
        {
            agent = (GOAP_Agent)target;

            CreteEditors(PlayModeStateChange.EnteredEditMode);

            EditorApplication.playModeStateChanged += CreteEditors;
        }

        private void CreteEditors(PlayModeStateChange status)
        {
            editors.Clear();
            foreach (var state in agent.worldStates)
            {
                if (!editors.TryGetValue(state.worldState.tag, out StateDataInspector editor))
                {
                    editor = CreateEditor(state) as StateDataInspector;
                    if (editor != null)
                    {
                        editor.Init();
                        editors.Add(state.worldState.tag, editor);
                    }
                }
            }
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            agent.behaviour = EditorGUILayout.ObjectField("GOAP Sheet", agent.behaviour, typeof(GOAP_Behaviour), false) as GOAP_Behaviour;

            GUILayout.BeginVertical("Information", "window", GUILayout.MinHeight(30));

            GUILayout.Label(agent.ToString());

            GUILayout.EndVertical();
            
            foreach (var state in agent.worldStates)
            {
                if (editors.TryGetValue(state.worldState.tag, out StateDataInspector editor))
                {
                    GUILayout.BeginVertical(state.worldState.tag, "window", GUILayout.MinHeight(10));
                    editor.DrawWorldState();
                    GUILayout.EndVertical();
                }
            }
        }
    }
}
