﻿using UnityEngine;
using UnityEditor;
using System.Linq;
using GOAP.WorldState;
using System.Text.RegularExpressions;

namespace GOAP.Editor
{
    [CustomEditor(typeof(StateData))]
    public class StateDataInspector : UnityEditor.Editor
    {
        public virtual void Init() { }

        public virtual void Draw()
        {
            GUILayout.Label("Inspector 'Draw' not implement!");
        }

        public virtual void DrawWorldState()
        {
            GUILayout.Label("Inspector 'DrawWorldState' not implement!");
        }
    }

    [CustomEditor(typeof(Numeric))]
    public class NumericInspector : StateDataInspector
    {
        Numeric instance;
        string text;
        readonly string[] icons = {"=",">","<",">=","<=" }; 

        public override void Init()
        {
            instance = target as Numeric;
            text = instance.value.ToString();
        }

        public override void DrawWorldState()
        {
            GUILayout.BeginHorizontal();

            GUILayout.Label("Current " + instance.worldState.tag + ": ");

            text = GUILayout.TextField(text);

            if (float.TryParse(text, out float v))
            {
                instance.value = v;
            }
            else
            {
                text = instance.value.ToString();
            }

            GUILayout.EndHorizontal();
        }

        public override void Draw()
        {
            void SetComparator(object i )
            {
                instance.comparator = (Numeric.Comparator)i;
            }

            GUILayout.BeginHorizontal();

            GUILayout.Label(instance.worldState.tag + ": ");

            if (GUILayout.Button(icons[(int)instance.comparator]))
            {
                GenericMenu genericMenu = new GenericMenu();


                for (int i = 0; i < icons.Length; i++)
                {
                    genericMenu.AddItem(new GUIContent(icons[i]), false, SetComparator, i);
                }

                genericMenu.ShowAsContext();
            }

            text = GUILayout.TextField(text);

            if(float.TryParse(text, out float v))
            {
                instance.value = v;
            }
            else
            {
                text = instance.value.ToString();
            }

            GUILayout.EndHorizontal();
        }
    }

    [CustomEditor(typeof(Boolean))]
    public class BooleanInspector : StateDataInspector
    {
        Boolean instance;

        public override void Init()
        {
            instance = target as Boolean;
        }

        public override void Draw()
        {
            GUILayout.BeginHorizontal();
            GUILayout.Label(instance.worldState.tag + " is : ");
            instance.value = GUILayout.Toggle(instance.value, " " + instance.value.ToString());
            GUILayout.EndHorizontal();
        }
    }

    [CustomEditor(typeof(Target))]
    public class TargetInspector : StateDataInspector
    {
        Target instance;

        int targetIndex;

        string[] aviableTargets;

        public override void Init()
        {
            instance = base.target as Target;

            var types = (from domainAssembly in System.AppDomain.CurrentDomain.GetAssemblies()
                            from assemblyType in domainAssembly.GetTypes()
                            where !assemblyType.IsAbstract && 
                            assemblyType.IsPublic &&
                            !assemblyType.IsGenericType && 
                            typeof(ITargetable).IsAssignableFrom(assemblyType) select assemblyType);

            if (aviableTargets == null)
            {
                aviableTargets = new string[types.Count()];
                for (int i = 0; i < types.Count(); i++)
                {
                    aviableTargets[i] = types.ElementAt(i).FullName.Replace('.', '/');

                    if (aviableTargets[i] == instance.target)
                        targetIndex = i;
                }
            }
        }

        public override void Draw()
        {
            if (aviableTargets.Count() > 0)
            {
                GUILayout.Label( instance.worldState.tag + ": " + aviableTargets[targetIndex].Split('/')[aviableTargets[targetIndex].Split('/').Length - 1]);
                targetIndex = EditorGUILayout.Popup(targetIndex, aviableTargets.ToArray(), GUILayout.MinWidth(50));
                instance.target = aviableTargets[targetIndex];
            }
            else
            {
                GUILayout.Label("You must have at least one class who implements 'ITargetable'");
            }
        }
    }
}
