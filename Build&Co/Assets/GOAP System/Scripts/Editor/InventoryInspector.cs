﻿using UnityEngine;
using UnityEditor;
using GOAP.ExampleGame;

namespace GOAP.Editor
{
    [CustomEditor(typeof(Inventory))]
    public class InventoryInspector : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            EditorGUILayout.LabelField((target as Inventory).ToString());
        }
    }
}