﻿using UnityEngine;
using UnityEditor;
using System;

namespace GOAP.Editor
{
    public class ActionNodeWindow : ScriptableObject
    {
        public ActionNode action;
        public string title;
        public Vector2 scroll_effect;
        public Vector2 scroll_precondition;
        public GUIStyle nodeStyle;
        private bool isDragged;

        public Rect window;

        Action <ActionNodeWindow> onRemove;
        Action<ActionNodeWindow> onInspect;

        public override bool Equals(object other)
        {
            if (other == null)
                return false;

            if(other is ActionNodeWindow)
            {
                return (action.actionType == (other as ActionNodeWindow).action.actionType);
            }

            return false;
        }

        public void Init(ActionNode action, Vector2 pos, Action<ActionNodeWindow> onRemove, Action<ActionNodeWindow> onInspect)
        {
            this.action = action;
            title = action.actionType.Split('.')[action.actionType.Split('.').Length - 1];
            scroll_effect = new Vector2();
            scroll_precondition = new Vector2();

            window = new Rect(pos.x, pos.y, 100, 50);

            nodeStyle = new GUIStyle();
            nodeStyle.alignment = TextAnchor.MiddleCenter;
            nodeStyle.normal.background = EditorGUIUtility.Load("builtin skins/darkskin/images/node1.png") as Texture2D;
            nodeStyle.border = new RectOffset(12, 12, 12, 12);

            this.onRemove = onRemove;
            this.onInspect = onInspect;
        }

        public void OnLoad(Action<ActionNodeWindow> onRemove, Action<ActionNodeWindow> onInspect)
        {
            this.onRemove = onRemove;
            this.onInspect = onInspect;
        }

        public void Draw()
        {
            GUIContent content = new GUIContent(title, action.actionType);

            GUI.Box(window,content,nodeStyle);
        }

        private void Select()
        {
            nodeStyle.normal.background = EditorGUIUtility.Load("builtin skins/darkskin/images/node1 on.png") as Texture2D;

            onInspect?.Invoke(this);
        }

        private void Deselect()
        {
            nodeStyle.normal.background = EditorGUIUtility.Load("builtin skins/darkskin/images/node1.png") as Texture2D;
        }

        public void ProcesssEvents(ref Event e)
        {
            switch (e.type)
            {
                case EventType.MouseDown:

                    if (e.button == 1 && window.Contains(e.mousePosition))
                    {
                        ProcessContextMenu();
                        e.Use();
                    }

                    if (e.button == 0)
                    {
                        if (window.Contains(e.mousePosition))
                        {
                            isDragged = true;

                            Select();
                        }
                        else
                        {
                            Deselect();
                        }

                        GUI.changed = true;
                    }
                    break;

                case EventType.MouseUp:
                    isDragged = false;
                    
                    break;

                case EventType.MouseDrag:
                    if (e.button == 0 && isDragged)
                    {
                        Drag(e.delta);
                        e.Use();
                    }
                    break;
            }
        }

        private void ProcessContextMenu()
        {
            GenericMenu genericMenu = new GenericMenu();

        //    foreach (Type action in AppDomain.CurrentDomain.GetAllDerivedTypes(typeof(GOAP_Action)))
         //   {
                genericMenu.AddItem(new GUIContent("Remove"), false, OnClickRemoveNode);
          //  }

            genericMenu.ShowAsContext();
        }

        public void Drag(Vector2 delta)
        {
            window.position += delta;
            GUI.changed = true;
        }

        private void OnClickRemoveNode()
        {
            onRemove?.Invoke(this);
            GUI.changed = true;
        }

        public override int GetHashCode()
        {
            return action.GetHashCode();
        }
    }
}
