﻿using UnityEngine;
using System.Collections;

namespace GOAP.ExampleGame
{
    [System.Serializable]
    public abstract class Item : ScriptableObject
    {
        public Sprite icon;
        public string item_name;
    }
}
