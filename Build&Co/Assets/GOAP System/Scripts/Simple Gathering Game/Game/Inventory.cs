﻿using UnityEngine;
using System.Collections.Generic;

namespace GOAP.ExampleGame
{
    public class Inventory : MonoBehaviour
    {
        List<ItemSlot> inventory = new List<ItemSlot>();

        public override string ToString()
        {
            string output = "Items in inventory: " + inventory.Count;

            foreach (var slot in inventory)
            {
                output += "\n" + slot.item.item_name + " = " + slot.amount; 
            }

            return output;
        }
    }

    public class ItemSlot
    {
        public Item item;
        public int amount;
    }
}