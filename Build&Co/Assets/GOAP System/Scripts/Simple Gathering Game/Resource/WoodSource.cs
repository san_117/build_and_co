﻿using UnityEngine;
using System.Collections;

namespace GOAP.ExampleGame
{
    public class WoodSource : MonoBehaviour, ITargetable
    {
        public int remaining = 10;
        public int GetWood()
        {
            if (remaining > 0)
            {
                remaining--;
                return 1;
            }
            else
            {
                return 0;
            }
        }
    }

}
