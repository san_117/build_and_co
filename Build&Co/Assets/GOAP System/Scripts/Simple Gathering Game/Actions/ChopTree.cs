using UnityEngine;

namespace GOAP.ExampleGame.Actions
{
    [RequireComponent(typeof(Inventory))]
    public class ChopTree : GOAP_Action
    {
        public Status status = Status.PERFORMING;

        public override void SetData(StateData data)
        {
           
        }

        public override Status Perform()
        {
            return status;
        }

        public override string Action()
        {
            return "Chop Tree";
        }
    }
}