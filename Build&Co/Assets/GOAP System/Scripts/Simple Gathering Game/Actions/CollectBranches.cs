using GOAP.WorldState;
using UnityEngine;

namespace GOAP.ExampleGame.Actions
{
    [RequireComponent(typeof(Inventory))]
    public class CollectBranches : GOAP_Action
    {
        WoodSource near_branch;
        float distance = Mathf.Infinity;
        float workAmount = 0;
        float totalWork = 4;
        StateData goal;

        public override void SetData(StateData data)
        {
            near_branch = null;
            goal = data;
            workAmount = 0;
            distance = Mathf.Infinity;
        }

        private WoodSource FindNearBranch()
        {
            WoodSource[] branches = FindObjectsOfType<Branch>();

            if (branches.Length == 0)
                return null;

            int near_index = 0;
            float near = Vector3.Distance(transform.position, branches[0].transform.position);

            for (int i = 0; i < branches.Length; i++)
            {
                float distance = Vector3.Distance(transform.position, branches[i].transform.position);
                if (distance < near)
                {
                    near = distance;
                    near_index = i;
                }
            }

            return branches[near_index];
        }

        public override Status Perform()
        {
            if (near_branch == null)
            {
                near_branch = FindNearBranch();
            }

            if (near_branch == null)
                return Status.ABORT;

            if(distance > 0.1f)
            {
                transform.position = Vector3.MoveTowards(transform.position, near_branch.transform.position, Time.deltaTime * 2.5f);
                distance = Vector3.Distance(transform.position, near_branch.transform.position);
            }
            else
            {
                workAmount += Time.deltaTime;
                if (workAmount >= totalWork)
                {
                    workAmount = 0;
                    int wood = near_branch.GetWood();

                    if (agent.CompareLocalDataToWorldData(goal))
                    {
                        return Status.COMPLETED;
                    }
                }
            }


            return Status.PERFORMING;
        }

        public override string Action()
        {
            return "Collect Branches";
        }
    }
}