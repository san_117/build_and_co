using UnityEngine;

namespace GOAP.ExampleGame.Actions
{
    public class FindTool : GOAP_Action
    {
        public override void SetData(StateData data)
        {

        }

        public override Status Perform()
        {
            return Status.ABORT;
        }
        public override string Action()
        {
            return "Find Tool";
        }

    }
}