using UnityEngine;

namespace GOAP.ExampleGame.Actions
{
    public class EquipTool : GOAP_Action
    {
        public override void SetData(StateData data)
        {

        }

        public override Status Perform()
        {
            return Status.ABORT;
        }

        public override string Action()
        {
            return "Equip Tool";
        }
    }
}