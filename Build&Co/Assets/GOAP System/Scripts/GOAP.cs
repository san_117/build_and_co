﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;

namespace GOAP { 

    [System.Serializable]
    public class ActionNode
    {
        public string actionType;
        public float weight;
        public List<StateData> preconditions = new List<StateData>();
        public List<StateData> effects = new List<StateData>();

        public ActionNode(string actionType)
        {
            this.actionType = actionType;
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            if (!(obj is ActionNode a))
                return false;

            return (a.actionType == actionType);
        }

        public override int GetHashCode()
        {
            var hashCode = 634818588;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(actionType);
            return hashCode;
        }
    }

    public abstract class StateData : ScriptableObject //WorldStateData
    {
        public WorldStates.State worldState;

        public abstract string UniqueKey();
        public override abstract bool Equals(object other);
        public abstract bool Compare(StateData other);
        public override abstract int GetHashCode();
        public virtual void Init(GOAP_Agent agent) { }

        public override string ToString()
        {
            string[] splited = worldState.tag.Split('/');

            return splited[splited.Length-1];
        }
    }

    [System.Serializable]
    public class WorldStates //WorldStateInfo
    {
        [System.Serializable]
        public class State
        {
            public string tag;
            public string type;

            public State(string tag, string type)
            {
                this.tag = tag;
                this.type = type;
            }
        }

        [SerializeField] private List<State> states = new List<State>();

        public int Size { get { return states.Count; } }

        public string[] GetProperties()
        {
            string[] propertiesNames = new string[states.Count];

            for (int i = 0; i < propertiesNames.Length; i++)
            {
                propertiesNames[i] = states[i].tag;
            }

            return propertiesNames;
        }

        public State GetProperty(int index)
        {
            if (index < states.Count)
            {
                return states[index];
            }

            return new State("new property", "InvalidType");
        }

        public bool AddPorperty(string property, string type)
        {
            for (int i = 0; i < states.Count; i++)
            {
                if (states[i].tag.ToLower() == property.ToLower())
                {
                    return false;
                }
            }

            states.Add(new State(property, type));
            return true;
        }

        public void RemovePropertyAt(int index)
        {
            states.RemoveAt(index);
        }
    }
   

    [System.Serializable]
    public class Astar
    {
        private static Queue<Task> Reconstruct(ref Dictionary<Node, Node> path, ref Node _node)
        {
            Queue<Task> nodes = new Queue<Task>();

            var node = _node;
            nodes.Enqueue(new Task(node.goal, node.action));

            while (true)
            {
                if (path.TryGetValue(node, out Node parent))
                {
                    nodes.Enqueue(new Task(parent.goal, parent.action));
                    node = parent;
                }
                else
                {
                    break;
                }
            }

            return nodes;
        }

        public static ActionChains[] GetPlans(StateData goal, Dictionary<string, StateData> worldState, GOAP_Action[] actions)
        {
            PriorityQueue<ActionChains> aux = new PriorityQueue<ActionChains>();
            ActionChains[] result;
            //if (worldState.TryGetValue(goal.worldState.tag, out StateData value))
            //{
            //    if (value.Compare(goal))
            //    {
            //        result = new ActionChains[1];
            //        result[0] = new ActionChains();
            //        result[0].Enqueue(new Task(goal, ));
            //        return result;
            //    }
            //}

            PriorityQueue<Node> priority = new PriorityQueue<Node>();

            foreach (GOAP_Action action in actions)
            {
                if (action.effects.TryGetValue(goal.UniqueKey(), out StateData value))
                {
                    if (goal.Equals(value))
                    {
                        Node root = new Node(value, action, action.weight, 0);
                        priority.Enqueue(root);
                    }
                }
            }

            while (priority.Count() > 0)
            {
                Node root = priority.Dequeue();

                if (root.action.preconditions.Count == 0)
                {
                    Task task = new Task(root.goal, root.action);
                    ActionChains chain = new ActionChains(task);
                    aux.Enqueue(chain);
                    continue;
                }
                else
                {
                    foreach (KeyValuePair<string, StateData> precondition in root.action.preconditions)
                    {
                        aux.Enqueue(FindPath(precondition.Value, root.action, worldState, actions));
                    }
                }
            }

            result = new ActionChains[aux.Count()];
            
            for (int i = 0; i < result.Length; i++)
            {
                ActionChains chain = aux.Dequeue();
                result[i] = chain;
            }

            return result;
        }

        private static ActionChains FindPath(StateData goal, GOAP_Action start_action, Dictionary<string, StateData> worldState, GOAP_Action[] aviableActions)
        {
            ActionChains chain;

            if (worldState.TryGetValue(goal.worldState.tag, out StateData value))
            {
                if (value.Compare(goal))
                {
                    Debug.Log("Alredy on goal");
                    chain = new ActionChains(new Task(goal, start_action));
                    return chain;
                }
            }

            _ = new Queue<GOAP_Action>();
            HashSet<Node> closed = new HashSet<Node>();
            PriorityQueue<Node> open = new PriorityQueue<Node>();
            HashSet<Node> open_set = new HashSet<Node>();
            Dictionary<Node, Node> path = new Dictionary<Node, Node>(new NodeComparer());

            int steps = 0;
            int maxIterations = 5000;

            //Insert all actions who can satisfy the effect
            foreach (GOAP_Action action in aviableActions)
            {
                if(action != start_action)
                {
                    if (action.effects.TryGetValue(goal.UniqueKey(), out value))
                    {
                        if (value.Compare(goal))
                        {
                            Node root = new Node(value, action, action.weight, steps);
                            open.Enqueue(root);
                            open_set.Add(root);
                        }
                    }
                }
            }

            Node defaultNode = open.Top();
            
            while (open.Count() > 0)
            {
                Node node = open.Dequeue();
                open_set.Remove(node);
                List<Node> neighbors = new List<Node>();

                if (node.action.preconditions.Count == 0)
                {
                    chain = new ActionChains(Reconstruct(ref path, ref node));
                    chain.Enqueue(new Task(node.goal, start_action));

                    return chain;
                }

                //Search if world states have the state data with the same value
                foreach (KeyValuePair<string, StateData> precondition in node.action.preconditions)
                {
                    foreach (var action in aviableActions)
                    {
                        if (worldState.TryGetValue(node.goal.worldState.tag, out value))
                        {
                            if (value.Compare(node.goal))
                            {
                                chain = new ActionChains(Reconstruct(ref path, ref node));
                                chain.Enqueue(new Task(node.goal, start_action));

                                return chain;
                            }
                        }
                    }
                }

                foreach (KeyValuePair<string, StateData> precondition in node.action.preconditions)
                {
                    foreach (var action in aviableActions)
                    {
                        if (action != node.action)
                        {
                            if (action.effects.TryGetValue(precondition.Value.UniqueKey(), out value))
                            {
                                neighbors.Add(new Node(precondition.Value, action, action.weight, steps));
                            }
                        }
                    }
                }

                foreach (Node neighbor in neighbors)
                {
                    if (closed.Contains(neighbor))
                    {
                        continue;
                    }

                    if (!open_set.Contains(neighbor))
                    {
                        open.Enqueue(neighbor);
                        open_set.Add(neighbor);
                        path[neighbor] = node;
                    }
                }

                closed.Add(node);
                steps++;

                if (steps > maxIterations)
                {
                    chain = new ActionChains(Reconstruct(ref path, ref defaultNode));
                    chain.Enqueue(new Task(node.goal, start_action));
                    return chain;
                }
            }

            chain = new ActionChains(Reconstruct(ref path, ref defaultNode));
            chain.Enqueue(new Task(goal, start_action));
            return chain;
        }
        
        public class ActionChains : System.IComparable<ActionChains>
        {
            private Queue<Task> chain = new Queue<Task>();
            private float totalWeight;

            public ActionChains(Task task)
            {
                Enqueue(task);
            }

            public ActionChains(Queue<Task> tasks)
            {
                Enqueue(tasks);
            }

            public Task GetTopTask()
            {
                return chain.Peek();
            }

            public Task NextTask()
            {
                Task task = chain.Dequeue();
                return task;
            }

            public int InQueue()
            {
                return chain.Count();
            }

            public override string ToString()
            {
                if (chain.Count == 0)
                {
                    return "No task in queue!";
                }

                return GetTopTask().ToString();
            }

            public void Enqueue(Task task)
            {
                totalWeight += task.action.weight;
                chain.Enqueue(task);
            }

            public void Enqueue(Queue<Task> tasks)
            {
                while (tasks.Count() > 0)
                {
                    Task aux = tasks.Dequeue();
                    totalWeight += aux.action.weight;
                    chain.Enqueue(aux);
                }
            }

            public float GetWeight()
            {
                return totalWeight;
            }

            public int CompareTo(ActionChains other)
            {
                if (totalWeight > other.totalWeight)
                {
                    return 1;
                }else if (totalWeight == other.totalWeight)
                {
                    return 0;
                }
                else
                {
                    return -1;
                }
            }
        }

        public class Task
        {
            public StateData data;
            public GOAP_Action action;

            public Task(StateData data, GOAP_Action action)
            {
                this.data = data;
                this.action = action;
            }

            public override string ToString()
            {
                return action.ToString();
            }
        }

        public class Node : System.IComparable<Node>
        {
            public float H;
            public float G;
            public float F;

            public StateData goal;
            public GOAP_Action action;

            public Node(StateData goal, GOAP_Action action, float heuristic, int steps)
            {
                this.action = action;
                this.goal = goal;
                H = heuristic;
                G = steps;
                F = G + H;
            }

            public int CompareTo(Node other)
            {
                if (F >= other.F)
                {
                    if (F == other.F)
                    {
                        if (G > other.G)
                        {
                            return 1;
                        }
                        else if (G == other.G)
                        {
                            return 0;
                        }
                        else
                        {
                            return -1;
                        }
                    }

                    return 1;
                }
                else
                {
                    return -1;
                }
            }
        }

        public class NodeComparer : EqualityComparer<Node>
        {
            public override bool Equals(Node x, Node y)
            {
                return x.goal.Equals(y.goal);
            }

            public override int GetHashCode(Node obj)
            {
                return obj.goal.GetHashCode();
            }
        }
    }
}

