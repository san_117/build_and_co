﻿using UnityEngine;
using System.Collections.Generic;

namespace GOAP
{
    public abstract class GOAP_Action : MonoBehaviour
    {
        protected GOAP_Agent agent;

        public enum Status
        {
            PERFORMING, COMPLETED, ABORT
        }

        public float weight;
        public Dictionary<string, StateData> preconditions = new Dictionary<string, StateData>();
        public Dictionary<string, StateData> effects = new Dictionary<string, StateData>();

        public void OnAsign(GOAP_Agent agent)
        {
            this.agent = agent;
        }

        public abstract void SetData(StateData data);
        public abstract Status Perform();

        public virtual string Action()
        {
            return GetType().Name;
        }

        public override string ToString()
        {
            return Action();
        }
    }
}
