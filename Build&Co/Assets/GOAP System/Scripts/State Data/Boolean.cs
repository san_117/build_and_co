﻿using UnityEngine;
using GOAP;

namespace GOAP.WorldState{

    public class Boolean : GOAP.StateData
    {
        public bool value = false;

        public override bool Equals(object other)
        {
            if (other == null)
                return false;

            if (other is Boolean)
            {
                Boolean t = other as Boolean;

                if (worldState.tag != t.worldState.tag)
                    return false;

                return (value == t.value);
            }
            return false;
        }

        public override bool Compare(GOAP.StateData other)
        {
            if (other == null)
                return false;

            if(other is Boolean)
            {
                return (value == (other as Boolean).value);
            }

            return false;
        }

        public override int GetHashCode()
        {
            var hashCode = 1737533024;
            hashCode = hashCode * -1521134295 + value.GetHashCode();
            return hashCode;
        }

        public override string UniqueKey()
        {
            return worldState.type + ":" + value.ToString();
        }

        public override string ToString()
        {
            if (value)
                return base.ToString();
            else
                return "Don't " + base.ToString();
        }
    }
}