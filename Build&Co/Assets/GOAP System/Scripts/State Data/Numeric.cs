﻿using UnityEngine;
using GOAP;

namespace GOAP.WorldState{

    public class Numeric : StateData
    {
        [System.Serializable]
        public enum Comparator
        {
            EQUALS,GREATER_THAN,LESS_THAN, GREATER_EQUALS_THAN, LESS_EQUALS_THAN
        }

        public float value = 0;
        public Comparator comparator = Comparator.EQUALS;

        public override bool Equals(object other)
        {
            if (other == null)
                return false;

            if (other is Numeric)
            {
                Numeric t = other as Numeric;

                if (worldState.tag != t.worldState.tag)
                    return false;

                return (value == t.value);
            }
            return false;
        }

        public override bool Compare(StateData other)
        {
            if (other == null)
                return false;

            if(other is StateData)
            {
                Numeric target = (other as Numeric);

                switch (comparator)
                {
                    case Comparator.EQUALS:
                        return value == target.value;
                    case Comparator.GREATER_THAN:
                        return value > target.value;
                    case Comparator.LESS_THAN:
                        return value < target.value;
                    case Comparator.GREATER_EQUALS_THAN:
                        return value >= target.value;
                    case Comparator.LESS_EQUALS_THAN:
                        return value <= target.value;
                    default: return false; 
                }
            }

            return false;
        }

        public override int GetHashCode()
        {
            var hashCode = Random.Range(1000,9999);
            return hashCode;
        }

        public override string UniqueKey()
        {
            return worldState.type + ":" + comparator.ToString() + " " + value;
        }

        public override string ToString()
        {
            string comparer;

            switch (comparator)
            {
                case Comparator.EQUALS:
                    comparer = "="; break;
                case Comparator.GREATER_THAN:
                    comparer = ">"; break;
                case Comparator.LESS_THAN:
                    comparer = "<"; break;
                case Comparator.GREATER_EQUALS_THAN:
                    comparer = ">="; break;
                case Comparator.LESS_EQUALS_THAN:
                    comparer = "<="; break;
                default: comparer = "=";break;
            }

            return base.ToString() + " " + comparer + " " + value;
        }
    }
}