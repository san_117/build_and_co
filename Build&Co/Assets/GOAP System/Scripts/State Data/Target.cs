﻿using UnityEngine;
using GOAP;
using System.Collections.Generic;

namespace GOAP.WorldState{

    public class Target : GOAP.StateData
    {
        public string target;

        public override bool Equals(object other)
        {
            if (other == null)
                return false;

            if (other is Target)
            {
                Target t = other as Target;

                if (worldState.tag != t.worldState.tag)
                    return false;

                return (target == t.target);
            }

            return false;
        }

        public override bool Compare(GOAP.StateData other)
        {
            if (other == null)
                return false;

            if (worldState.tag != other.worldState.tag)
                return false;

            if (other is Target)
            {
                Target t = other as Target;

                return (target == t.target);
            }

            return false;
        }

        public override int GetHashCode()
        {
            var hashCode = 1469209310;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(target);
            return hashCode;
        }

        public override string UniqueKey()
        {
            return worldState.tag + ":" + target;
        }

        public override string ToString()
        {
            string[] t = target.Split('/');
            return base.ToString() + "->" + t[t.Length-1];
        }
    }
}