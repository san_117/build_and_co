﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraConstructionSiteCenter: MonoBehaviour
{
    public Vector3 offset;

    [SerializeField] ConstructionSite site;
    
    void Start()
    {
        ReCenter();
    }

    [ContextMenu("Re Center")]
    public void ReCenter()
    {
        float x = site.current.widht / 2;
        float y = site.current.lenght / 2;
        float z = (site.current.underheight + site.current.height) / 2;

        Vector3 finalPos = new Vector3(x, y, z) + offset;

        this.transform.position = finalPos;
    }
}
