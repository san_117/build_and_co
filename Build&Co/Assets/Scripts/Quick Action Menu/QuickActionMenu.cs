﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuickActionMenu : MonoBehaviour
{
    [SerializeField] private QuickActionItem quickActionUI_template;
    [SerializeField] private GameObject panel;
    [SerializeField] private Vector2 offset;

    private void Start()
    {
        quickActionUI_template.gameObject.SetActive(false);
    }

    public void Show(QuickAction[] actions)
    {
        gameObject.SetActive(true);

        Clear();
        panel.transform.position = Input.mousePosition + (Vector3)offset;

        for (int i = 0; i < actions.Length; i++)
        {
            QuickActionItem quickAction = Instantiate(quickActionUI_template.gameObject, panel.transform).GetComponent<QuickActionItem>();
            quickAction.text_component.text = actions[i].menuName;
            quickAction.action = actions[i].action;
            quickAction.name = actions[i].menuName;
            quickAction.gameObject.SetActive(true);
        }
    }

    private void Clear()
    {
        foreach (Transform child in panel.transform)
        {
            if (child.gameObject.name != quickActionUI_template.name)
            {
                Destroy(child.gameObject);
            }
        }
    }

    public struct QuickAction
    {
        public System.Action action;
        public string menuName;

        public QuickAction(Action action, string menuName)
        {
            this.action = action;
            this.menuName = menuName;
        }
    }
}
