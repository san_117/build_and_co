﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuickActionItem : MonoBehaviour
{
    public Text text_component;
    public System.Action action;


    public void Select()
    {
        action.Invoke();

        transform.parent.parent.gameObject.SetActive(false);
    }
}
