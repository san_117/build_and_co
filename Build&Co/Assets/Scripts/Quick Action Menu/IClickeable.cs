﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IClickeable
{
    void OnPointerEnter(Vector3 hitPos);
    void OnPointerOver(Vector3 hitPos, Vector3 normal);
    void OnPointerExit(Vector3 hitPos);
    void Click(int mouseButtonIndex, Vector3 hitPos);
}
