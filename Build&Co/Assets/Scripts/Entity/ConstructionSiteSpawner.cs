﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static ConstructionSite.Layer;

public class ConstructionSiteSpawner : MonoBehaviour
{
    public ConstructionSite site;
    public Entity entity;
    public void Spawn(Vector3 tilePos)
    {
        Entity.Spawn(site.GetTile(tilePos), entity);
    }
}
