﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static ConstructionSite.Layer;
using GOAP;

public abstract class Entity : MonoBehaviour, IQuickActionable
{
    [SerializeField] Tile current;

    [SerializeField] GameObject over_selector;

    public float speed = 1;

    public static void Spawn(Tile tile, Entity prefab)
    {
        Entity entity = Instantiate(prefab);
        entity.Teleport(tile);
    }

    public void Teleport(Tile tile)
    {
        current = tile;
        transform.position = tile.TopPos();
    }

    public void RandomMove()
    {
        StopAllCoroutines();

        Tile randomPos = current.GetLayer().GetAllTiles()[Random.Range(0, current.GetLayer().GetAllTiles().Count-1)];
        Queue<Tile> path = Pathfinder.GetPath(current, randomPos);

        StartCoroutine(AnimateMove(path, speed));
    }

    private IEnumerator AnimateMove(Queue<Tile> path, float speed)
    {
        Queue<Tile> temp = path;

        print(path.Count);

        while (path.Count > 0)
        {
            Tile actual = path.Dequeue();
            actual.GetBlock().SetMark(true);

            while (Vector3.Distance(transform.position, actual.TopPos()) > 0.2f)
            {
                transform.position = Vector3.MoveTowards(transform.position, actual.TopPos(), Time.deltaTime * speed);
                yield return new WaitForSeconds(Time.deltaTime);
            }

            current = actual;

            yield return new WaitForSeconds(Time.deltaTime);
            actual.GetBlock().SetMark(false);
        }

        RandomMove();
    }

    public QuickActionMenu.QuickAction[] ShowQuickActionMenu()
    {
        QuickActionMenu.QuickAction[] action = new QuickActionMenu.QuickAction[1];
        action[0] = new QuickActionMenu.QuickAction(RandomMove, "Random Move");

        return action;
    }

    public void OnPointerEnter(Vector3 hitPos)
    {
        over_selector.SetActive(true);
    }

    public void OnPointerOver(Vector3 hitPos, Vector3 normal)
    {
        
    }

    public void OnPointerExit(Vector3 hitPos)
    {
        over_selector.SetActive(false);
    }

    public void Click(int mouseButtonIndex, Vector3 hitPos)
    {
        
    }
}
