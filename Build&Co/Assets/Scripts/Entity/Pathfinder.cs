﻿using UnityEngine;
using System.Collections.Generic;
using static ConstructionSite.Layer;
using static ConstructionSite;
using System.Linq;

public static class Pathfinder
{
    private static Queue<Tile> Reconstruct(ref Dictionary<Node,Node> path, ref Node _node, Layer layer)
    {
        Queue<Tile> nodes = new Queue<Tile>();

        var node = _node;
        nodes.Enqueue(layer.GetTile(node.coord));
        int tries = path.Count*2;
        while (tries > 0)
        {
            if (path.TryGetValue(node, out Node parent))
            {
                nodes.Enqueue(layer.GetTile(parent.coord));
                node = parent;
            }
            else
            {
                break;
            }

            tries--;
        }

        nodes = new Queue<Tile>(nodes.Reverse());
        return nodes;
    }

    /// <summary>
    /// Finds a path between 2 tiles with A* algorith
    /// </summary>
    /// <param name="from"></param>
    /// <param name="to"></param>
    /// <returns></returns>
    public static Queue<Tile> GetPath(Tile from, Tile to)
    {
        Queue<Tile> result = new Queue<Tile>();

        var startTime = Time.realtimeSinceStartup;

        Layer layer = from.GetLayer();

        if (from.GetCoords() == to.GetCoords())
        {
            return result;
        }
     
        if (!from.GetBlock().blockData.isWalkable || !to.GetBlock().blockData.isWalkable)
        {
            return result;
        }     

        float GetRawDistance(GridCoord start, GridCoord end)
        {
            return ( Mathf.Pow((start.x - end.x), 2) + Mathf.Pow((start.z - end.z), 2));
        }

        HashSet<Node> closed = new HashSet<Node>();
        PriorityQueue<Node> open = new PriorityQueue<Node>();
        HashSet<Node> open_set = new HashSet<Node>();
        Dictionary<Node, Node> path = new Dictionary<Node, Node>(new NodeComparer());

        int steps = 0;
        int maxIterations = 120000;

        Node root = new Node(from.GetCoords(), GetRawDistance(from.GetCoords(), to.GetCoords()) , steps);

        open.Enqueue(root);
        open_set.Add(root);

        while (open.Count()>0 && steps < maxIterations)
        {
            Node node = open.Top();
            open.Pop();
            open_set.Remove(node);

            if (node.coord == to.GetCoords()) 
            {
                result = Reconstruct(ref path, ref node, layer);

                var end = Time.realtimeSinceStartup;
                var diff = (end - startTime);
                var nodeCount = closed.Count;
                var solution = result.Count;

            //    Debug.Log("Time: " + diff + "\n" + "Nodes: " + nodeCount + "\n" + "Solution: " + solution);

                return result;
            }

            List<Node> neighbors = new List<Node>();

            foreach (Tile neighbor in layer.GetTile(new GridCoord((int)node.coord.x, layer.GetLevel(), (int)node.coord.z)).GetNeighborsAllNeighbors2D())
            {
                if (neighbor.GetBlock().blockData.isWalkable)
                {
                    Tile upTile = neighbor.GetLayer().ConstructionSite.GetTile(new GridCoord(neighbor.GetCoords().x, neighbor.GetCoords().y+1, neighbor.GetCoords().z));

                    if (upTile != null)
                    {
                        if (upTile.GetBlock().blockData.isTransparent)
                        {
                            Node child = new Node(neighbor.GetCoords(), GetRawDistance(neighbor.GetCoords(), to.GetCoords()), steps);
                            neighbors.Add(child);
                        }
                    }
                    else
                    {
                        Node child = new Node(neighbor.GetCoords(), GetRawDistance(neighbor.GetCoords(), to.GetCoords()), steps);
                        neighbors.Add(child);
                    }
                }
            }

            foreach (Node neighbor in neighbors)
		    {
                if (closed.Contains(neighbor))
                {
                    continue;
                }

                if (!open_set.Contains(neighbor))
                {
                    open.Enqueue(neighbor);
                    open_set.Add(neighbor);
                    path[neighbor] = node;
                }
            }

            closed.Add(node);
            steps++;
        }

     //   Debug.Log("Time: " + (Time.realtimeSinceStartup - startTime) + "\n" + "Nodes: " + closed.Count + "\n" + "Solution: -1");

        result.Enqueue(from);

        return result;
    }

    public class Node: System.IComparable<Node>
    {
        public float H;
        public float G;
        public float F;

        public GridCoord coord;

        public Node(GridCoord coord, float heuristic, int steps)
        {
            this.coord = coord;
            H = heuristic;
            G = steps;
            F = G + H;
        }

        public int CompareTo(Node other)
        {
            if (F >= other.F)
            {
                if (F == other.F)
                {
                    if (G > other.G)
                    {
                        return 1;
                    }else if (G == other.G)
                    {
                        return 0;
                    }
                    else
                    {
                        return -1;
                    }
                }

                return 1;
            }
            else
            {
                return -1;
            }
        }
    }

    public class NodeComparer : EqualityComparer<Node>
    {
        public override bool Equals(Node x, Node y)
        {
            return x.GetHashCode() == y.GetHashCode();
        }

        public override int GetHashCode(Node obj)
        {
            return obj.coord.GetHashCode();
        }
    }
}




