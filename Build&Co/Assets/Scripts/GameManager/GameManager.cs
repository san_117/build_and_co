﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private static GameManager _singleton;
    public QuickActionMenu quickActionHandler;

    //Cursor interaction cache
    private IClickeable lastClickableElement;

    public static GameManager Singleton
    {
        get
        {
            if (_singleton == null)
                _singleton = FindObjectOfType<GameManager>();

            return _singleton;
        }
    }

    private static ConstructionSite _constructionSite;

    public static ConstructionSite CurrentConstructionSite
    {
        get
        {
            if (_constructionSite == null)
            {
                _constructionSite = FindObjectOfType<ConstructionSite>();

                if (_constructionSite == null)
                    Debug.Log("Cant find any construction site in scene " + UnityEngine.SceneManagement.SceneManager.GetActiveScene().name);
            }

            return _constructionSite;
        }
    }

    public void Update()
    {
        CursorInteractions();
    }

    private void CursorInteractions()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out RaycastHit hit))
        {
            IClickeable objectHit = hit.transform.gameObject.GetComponent<IClickeable>();

            if (objectHit != null)
            {
                if (lastClickableElement == null)
                {
                    lastClickableElement = objectHit;
                    lastClickableElement.OnPointerEnter(hit.point);
                }
                else
                {
                    if (lastClickableElement != objectHit)
                    {
                        lastClickableElement.OnPointerExit(hit.point);
                        lastClickableElement = objectHit;
                        lastClickableElement.OnPointerEnter(hit.point);
                    }
                }

                objectHit.OnPointerOver(hit.point, hit.normal);

                if (Input.GetMouseButtonDown(1))
                {
                    if (lastClickableElement is IQuickActionable quickAction)
                    {
                        QuickActionMenu.QuickAction[] actions = quickAction.ShowQuickActionMenu();
                        quickActionHandler.Show(actions);
                    }
                }
            }
        }
    }
}
