﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameConsole
{
    private static Dictionary<string, string> _headSiteCommand;

    private static Dictionary<string, string> headSiteCommand
    {
        get
        {
            if (_headSiteCommand == null)
            {
                _headSiteCommand = new Dictionary<string, string>();
                _headSiteCommand.Add("layer", "layer [up,down]");
                _headSiteCommand.Add("tileMove", "tileMove [true, false]");
            }

            return _headSiteCommand;
        }
    }

    private static Dictionary<string, string> _headHelpCommand;

    private static Dictionary<string, string> headHelpCommand
    {
        get
        {
            if (_headHelpCommand == null)
            {
                _headHelpCommand = new Dictionary<string, string>();
                _headHelpCommand.Add("help", "show aviable commands");
                _headHelpCommand.Add("site", "construction site commands");
            }

            return _headHelpCommand;
        }
    }

    public static void RecieveCommand(string command)
    {
        string[] argv = command.Split(' ');

        if (argv.Length <= 0)
            return;

        switch (argv[0])
        {
            case "help": ShowAviableCommands(headHelpCommand); break;
            case "site": SiteCommand(argv); break;
            default: Debug.Log("Command not found, try 'help'"); break;
        }
    }

    private static void SiteCommand(string[] argv)
    {
        string warning_layer = "Syntax error, Use -> site layer [up,down]";

        if (argv.Length == 1)
        {
            ShowAviableCommands(headSiteCommand);
            return;
        }

        switch (argv[1])
        {
            case "layer":

                if (argv.Length == 2)
                {
                    Debug.Log(warning_layer);
                }else if (argv.Length == 3)
                {
                    if (argv[2] == "up")
                    {
                        GameManager.CurrentConstructionSite.UpLayer();
                    }
                    else if (argv[2] == "down")
                    {
                        GameManager.CurrentConstructionSite.DownLayer();
                    }
                    else
                    {
                        Debug.Log(warning_layer);
                    }
                }
                else
                {
                    Debug.Log(warning_layer);
                }

                break;
            case "tileMove":
                if (argv.Length == 2)
                {
                    if (argv[2] == "true")
                    {
                      //  GameManager.Singleton.TemporalMove(true);
                    }
                    else if(argv[2] == "false")
                    {
                     //   GameManager.Singleton.TemporalMove(false);
                    }
                }
                    break;
        }
    }

    static void ShowAviableCommands(Dictionary<string, string> headCommand)
    {
        string output = string.Empty;

        foreach (KeyValuePair<string, string> entry in headCommand)
        {
            output += entry.Key + " - " + entry.Value + "\n";
        }

        Debug.Log(output);
    }
}
