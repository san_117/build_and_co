﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;

public class ConstructionSiteGenerator : MonoBehaviour
{
    public string path = "Assets/Resources/GameData/Campaign/Campaign_1/Level.dat";

    [Space]

    public int widht;
    public int height;
    public int underheight;
    public int lenght;

    public List<LayerInfo> layersInfo = new List<LayerInfo>(); 

    [ContextMenu("Create File")]
    public void CreateFile()
    {
        List<ConstructionSiteData.LayerData> layers = new List<ConstructionSiteData.LayerData>();

        for (int i = 0; i < height; i++)
        {
            layers.Add(new ConstructionSiteData.LayerData(widht, lenght, height - i, "0:0"));
        }

        layers.Add(new ConstructionSiteData.LayerData(widht, lenght, 0 , "2:0"));

        int underLevel = -1;

        for (int i = 0; i < layersInfo.Count; i++)
        {
            for (int e = 0; e < layersInfo[i].layerCount; e++)
            {
                layers.Add(new ConstructionSiteData.LayerData(widht, lenght, underLevel, layersInfo[i].blockData.id));
                underLevel--;
            }
        }

        ConstructionSiteData current = new ConstructionSiteData(widht, lenght, height, underheight, layers.ToArray());

        string json = JsonUtility.ToJson(current);

        using (FileStream fs = new FileStream(path, FileMode.Create))
        {
            using (StreamWriter writer = new StreamWriter(fs))
            {
                writer.Write(json);
            }
        }

#if UNITY_EDITOR
        UnityEditor.AssetDatabase.Refresh();
#endif
    }

    [System.Serializable]
    public class LayerInfo
    {
        public BlockData blockData;
        public float percentage;
        public int layerCount;
    }
}
