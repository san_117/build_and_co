﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static ConstructionSite.Layer;

[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer), typeof(BoxCollider))]
public class Block : MonoBehaviour, IQuickActionable
{
    public Tile tile;
    public BlockData blockData;

    [SerializeField] GameObject over_selector;
    [SerializeField] GameObject mark;

    public void Init(Tile tile)
    {
        this.tile = tile;
        blockData = GameAssets.GetBlock(tile.TileData.blockID);
        //Añadir componentes en lista
    }

    public void Render()
    {
        if (blockData.isTransparent)
        {
            GetComponent<Collider>().enabled = false;
            GetComponent<MeshFilter>().mesh = null;
        }
        else
        {
            GetComponent<Collider>().enabled = true;
            MeshFilter mf = GetComponent<MeshFilter>();
            MeshRenderer mr = GetComponent<MeshRenderer>();

            mf.mesh = blockData.mesh;
            mr.material = blockData.mat;
        }

        //Aqui actualizo los componentes
    }

    [ContextMenu("Build Block")]
    public void BuildBlock()
    {
        //Set Build;
        Render();

        //Update Render of 4 Neighbors
        foreach (Tile neighbor in tile.GetDirectNeighbors2D())
        {
            neighbor.GetBlock().Render();
        }

        tile.Culling();
    }

    [ContextMenu("Destroy Block")]
    public void DestroyBlock()
    {
        tile.TileData.blockID = "0:0";
        Init(tile);
        BuildBlock();

        //REMOVER COMPONENTES
    }

    public void SetMark(bool value)
    {
        mark.SetActive(value);
    }

    public void OnPointerEnter(Vector3 hitPoint)
    {
        over_selector.gameObject.SetActive(true);
    }

    public void OnPointerExit(Vector3 hitPoint)
    {
        over_selector.gameObject.SetActive(false);
    }

    public void OnPointerOver(Vector3 hitPoint, Vector3 normal)
    {
        Vector3 incomingVec = normal - Vector3.up;
        
        //South
        if (incomingVec == new Vector3(0, -1, -1))
        {
            over_selector.transform.localPosition = new Vector3(0,0.5f,-0.51f);
            over_selector.transform.localRotation = Quaternion.Euler(0, 0, 0);
        }

        //North
        if (incomingVec == new Vector3(0, -1, 1))
        {
            over_selector.transform.localPosition = new Vector3(0,0.5f,0.51f);
            over_selector.transform.localRotation = Quaternion.Euler(0,0,0);
        }

        //Up
        if (incomingVec == new Vector3(0, 0, 0))
        {
            over_selector.transform.localPosition = new Vector3(0,1.01f,0);
            over_selector.transform.localRotation = Quaternion.Euler(90,0,0);
        }

        //Down
        if (incomingVec == new Vector3(1, 1, 1))
        {
            over_selector.transform.localPosition = new Vector3(0, -0.01f, 0);
            over_selector.transform.localRotation = Quaternion.Euler(90, 0, 0);
        }

        //West
        if (incomingVec == new Vector3(-1, -1, 0))
        {
            over_selector.transform.localPosition = new Vector3(-0.51f, 0.5f, 0);
            over_selector.transform.localRotation = Quaternion.Euler(0, 90, 0);
        }

        //East
        if (incomingVec == new Vector3(1, -1, 0))
        {
            over_selector.transform.localPosition = new Vector3(0.51f, 0.5f, 0);
            over_selector.transform.localRotation = Quaternion.Euler(0, 90, 0);
        }
    }

    QuickActionMenu.QuickAction[] IQuickActionable.ShowQuickActionMenu()
    {
        QuickActionMenu.QuickAction[] action = new QuickActionMenu.QuickAction[4];
        action[0] = new QuickActionMenu.QuickAction(DestroyBlock, "Destroy");
        action[1] = new QuickActionMenu.QuickAction(tile.GetLayer().ConstructionSite.UpLayer, "Up Layer");
        action[2] = new QuickActionMenu.QuickAction(tile.GetLayer().ConstructionSite.DownLayer, "Down Layer");
        action[3] = new QuickActionMenu.QuickAction(TempSpawn, "Spawn");

        return action;
    }

    private void TempSpawn()
    {
        tile.GetLayer().ConstructionSite.transform.GetComponent<ConstructionSiteSpawner>().Spawn(tile.GetCoords());
    }

    public void Click(int mouseButtonIndex, Vector3 clickpos)
    {
        
    }
}
