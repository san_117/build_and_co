﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class ConstructionSite : MonoBehaviour
{
    public string loadFromPath = "Assets/Resources/GameData/Campaign/Campaign_1/Level.dat";
    public bool load = true;
    public ConstructionSiteData current;

    public Dictionary<int, Layer> layers = new Dictionary<int, Layer>();

    //Game Data
    private int layerView = 0;

    private void Start()
    {
        if (load)
        {
            if (TryLoad())
            {
                Build();
            }
            else
            {
                print("Current site is null!");
                return;
            }
        }
        else
        {
            Build();
        }
    }

    [ContextMenu("Try Load")]
    private bool TryLoad()
    {
        string content = string.Empty;

        using (FileStream fs = new FileStream(loadFromPath, FileMode.Open))
        {
            using (StreamReader writer = new StreamReader(fs))
            {
                content = writer.ReadToEnd();
            }
        }

        current = JsonUtility.FromJson<ConstructionSiteData>(content);

        return (current != null);
    }

    public void Build()
    {
        List<Layer> initQueue = new List<Layer>();

        for (int i = 0; i < current.layers.Length; i++)
        {
            Layer aux = new Layer(this, ref current.layers[i], this.transform);
            layers.Add(current.layers[i].level, aux);
            initQueue.Add(aux);
        }

        for (int i = 0; i < initQueue.Count; i++)
        {
            initQueue[i].InitNeighbors();
            initQueue[i].InitLayer();
        }

        for (int i = 0; i < initQueue.Count; i++)
        {
            for (int e = 0; e < initQueue[i].GetAllTiles().Count; e++)
            {
                initQueue[i].GetAllTiles()[e].Culling();
            }
        }
    }

    public Layer GetLayer(int level)
    {
        return layers[level];
    }

    public  Layer.Tile GetTile(GridCoord coords)
    {
        return layers[coords.y].GetTile(coords);
    }

    public void UpLayer()
    {
        if (layerView == current.height)
            return;

        layerView++;
        layers[layerView].ShowLayer();

        foreach (Layer.Tile tile in layers[layerView - 1].GetAllTiles())
        {
            tile.Culling();
        }
    }

    public void DownLayer()
    {
        if (layerView == -current.underheight)
            return;

        layers[layerView].HideLayer();

        layerView--;

        layers[layerView].ShowLayer();
    }

    public class Layer
    {
        private ConstructionSiteData.LayerData layerData;
        private Dictionary<GridCoord, Tile> tiles = new Dictionary<GridCoord, Tile>(new GridCoordComparer());
        private readonly Transform transform;

        private Action onLayersBuilded;
        private Action onInitLayers;

        public Layer(ConstructionSite site, ref ConstructionSiteData.LayerData layerData, Transform parent)
        {
            this.layerData = layerData;
            this.ConstructionSite = site;

            transform = new GameObject("Layer " + layerData.level).transform;
            transform.SetParent(parent);
            transform.transform.localPosition = new Vector3(0, layerData.level * Tile.Size, 0);

            foreach (ConstructionSiteData.LayerData.TileData data in layerData.tiles)
            {
                Tile tile = new Tile(data, this, ref onLayersBuilded, ref onInitLayers);
                tiles.Add(data.coord, tile);
            }
        }

        /// <summary>
        /// Called after all layers are created, find all neighbors of each tile.
        /// </summary>
        public void InitNeighbors()
        {
            onLayersBuilded.Invoke();
        }

        //Final call after all layers are created, build all blocks.
        public void InitLayer()
        {
            onInitLayers.Invoke();
        }

        public void HideLayer()
        {
            foreach (Tile tile in GetAllTiles())
            {
                tile.Hide();
            }
        }

        public void ShowLayer()
        {
            foreach (Tile tile in GetAllTiles())
            {
                tile.Show();
            }
        }

        /// <summary>
        /// Get all the tiles in  layer
        /// </summary>
        /// <returns></returns>
        public List<Tile> GetAllTiles()
        {
            List<Tile> aux = new List<Tile>();

            foreach (KeyValuePair<GridCoord, Tile> tile in tiles)
            {
                aux.Add(tile.Value);
            }

            return aux;
        }

        public ConstructionSite ConstructionSite { get; private set; }

        public int GetLevel()
        {
            return layerData.level;
        }

        public Tile GetTile(GridCoord coords)
        {
            return tiles[coords];
        }

        [System.Serializable]
        public class Tile
        {
            public static readonly float Size = 1;

            private readonly Block block;
            private readonly Layer layer;

            private readonly List<Tile> allNeighbors2D = new List<Tile>();
            private readonly List<Tile> directNeighbors2D = new List<Tile>();
            private List<Tile> directNeighbors3D = new List<Tile>();

            public ConstructionSiteData.LayerData.TileData TileData { get; private set; }

            public bool IsCulling { get; private set; }

            public Vector3 TopPos()
            {
                return new Vector3(block.transform.position.x, block.transform.position.y + Size, block.transform.position.z);
            }

            public Block GetBlock()
            {
                return block;
            }

            public Layer GetLayer()
            {
                return layer;
            }

            public void Hide()
            {
                IsCulling = true;
                block.gameObject.SetActive(false);
            }

            public void Show()
            {
                IsCulling = false;
                block.gameObject.SetActive(true);
            }

            public void Culling()
            {
                foreach (Tile n in directNeighbors3D)
                {
                    if (n.GetNeighborsDirectNeighbors3D().Count == 6)
                    {
                        bool isSorounded = true;

                        foreach (Tile t in n.GetNeighborsDirectNeighbors3D())
                        {
                            if (t.block.blockData.isTransparent || t.IsCulling)
                            {
                                isSorounded = false;
                                break;
                            }
                        }

                        if (isSorounded || n.block.blockData.isTransparent)
                        {
                            n.Hide();
                        }
                        else
                        {
                            n.Show();
                        }
                    }
                }
            }

            public GridCoord GetCoords()
            {
                return TileData.coord;
            }

            private void FindNeighbors()
            {
                for (int z = -1; z < 2; z++)
                {
                    for (int x = -1; x < 2; x++)
                    {
                        if (!((x == 0) && (z == 0)))
                        {
                            GridCoord next = new GridCoord(TileData.coord.x + x, TileData.coord.y ,TileData.coord.z + z);

                            if ((next.x >= 0 && next.x < layer.layerData.widht) && (next.z >= 0 && next.z < layer.layerData.height))
                            {
                                if (x == -1 && z == 0)
                                {
                                    directNeighbors2D.Add(layer.GetTile(next));
                                }

                                if (x == 1 && z == 0)
                                {
                                    directNeighbors2D.Add(layer.GetTile(next));
                                }

                                if (x == 0 && z == +1)
                                {
                                    directNeighbors2D.Add(layer.GetTile(next));
                                }

                                if (x == 0 && z == -1)
                                {
                                    directNeighbors2D.Add(layer.GetTile(next));
                                }

                                allNeighbors2D.Add(layer.GetTile(next));
                            }
                        }
                    }
                }

                directNeighbors3D = new List<Tile>(directNeighbors2D);

                int topLayerLevel = layer.GetLevel() + 1;
                int downLayerLevel = layer.GetLevel() - 1;

                if (topLayerLevel <= layer.ConstructionSite.current.height)
                {
                    Layer topLayer = layer.ConstructionSite.GetLayer(topLayerLevel);
                    directNeighbors3D.Add(topLayer.GetTile(new GridCoord(TileData.coord.x, topLayerLevel, TileData.coord.z)));
                }

                if (downLayerLevel >= -layer.ConstructionSite.current.underheight)
                {
                    Layer downLayer = layer.ConstructionSite.GetLayer(downLayerLevel);
                    directNeighbors3D.Add(downLayer.GetTile(new GridCoord(TileData.coord.x, downLayerLevel, TileData.coord.z)));
                }
            }

            public List<Tile> GetNeighborsAllNeighbors2D()
            {
                return allNeighbors2D;
            }

            public List<Tile> GetDirectNeighbors2D()
            {
                return directNeighbors2D;
            }

            public List<Tile> GetNeighborsDirectNeighbors3D()
            {
                return directNeighbors3D;
            }

            public Tile(ConstructionSiteData.LayerData.TileData tileData, Layer layer, ref Action onLayersBuilded, ref Action onInitLayers)
            {
                this.TileData = tileData;
                this.layer = layer;

                Block prefab = Resources.Load<Block>("Prefabs/Block/Block");

                //Generate Tile
                this.block = Instantiate(prefab.gameObject, layer.transform).GetComponent<Block>();
                this.block.gameObject.name = "Tile " + tileData.coord.x + "," + tileData.coord.y + "," + tileData.coord.z;
                this.block.Init(this);

                //Init Tile
                this.block.Init(this);

                block.transform.localPosition = new Vector3(tileData.coord.x * Tile.Size, 0, tileData.coord.z * Tile.Size);

                onLayersBuilded += FindNeighbors;
                onInitLayers += this.block.BuildBlock;
            }
        }
    }
}