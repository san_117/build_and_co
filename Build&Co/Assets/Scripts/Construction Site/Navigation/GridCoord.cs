﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct GridCoord
{
    public int x;
    public int y;
    public int z;

    public GridCoord(int x, int y, int z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public override bool Equals(object obj)
    {
        if (!(obj is GridCoord))
        {
            return false;
        }

        var coord = (GridCoord)obj;
        return x == coord.x &&
               y == coord.y &&
               z == coord.z;
    }

    public override string ToString()
    {
        return (x + "," + y + "," + z); 
    }

    public override int GetHashCode()
    {
        var hashCode = x ^ y * 137 ^ z * 11317;
        return hashCode;
    }

    public static bool operator ==(GridCoord a, GridCoord b)
    {
        return (a.x == b.x && a.y == b.y && a.z == b.z);
    }

    public static bool operator !=(GridCoord a, GridCoord b)
    {
        return (a.x != b.x  || a.y != b.y || a.z != b.z);
    }

    public static implicit operator Vector3(GridCoord rValue)
    {
        return new Vector3(rValue.x, rValue.y, rValue.z);
    }

    public static implicit operator GridCoord(Vector3 rValue)
    {
        return new GridCoord((int)rValue.x, (int)rValue.y, (int)rValue.z);
    }
}

public class GridCoordComparer : EqualityComparer<GridCoord>
{
    public override bool Equals(GridCoord a1, GridCoord a2)
    {
        return a1.Equals(a2);
    }

    public override int GetHashCode(GridCoord a)
    {
        return a.GetHashCode();
    }
}
