﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(ConstructionSiteGenerator))]
public class ConstruccionSiteGeneratorInspector : Editor
{
    ConstructionSiteGenerator site;

    private void OnEnable()
    {
        site = (ConstructionSiteGenerator)target;
    }

    public override void OnInspectorGUI()
    {
        site.path = EditorGUILayout.TextField("Save Path", site.path);

        site.widht = EditorGUILayout.IntSlider("Widh", site.widht, 1, 64);
        site.lenght = EditorGUILayout.IntSlider("Lenght", site.lenght, 1, 64);
        site.height = EditorGUILayout.IntSlider("Height", site.height, 0, 16);
        site.underheight = EditorGUILayout.IntSlider("Underheight", site.underheight, 0, 16);
        int layerCount = 0;
        for (int i = 0; i < site.layersInfo.Count; i++)
        {
            EditorGUILayout.BeginHorizontal();

            site.layersInfo[i].blockData = (BlockData) EditorGUILayout.ObjectField(site.layersInfo[i].blockData, typeof(BlockData), false);

            if (i == 0)
            {
                float min = 0;
                EditorGUILayout.MinMaxSlider(ref min, ref site.layersInfo[i].percentage, 0, 1, GUILayout.MinWidth(200));
                site.layersInfo[i].layerCount = Mathf.RoundToInt(site.layersInfo[i].percentage * site.underheight);
                EditorGUILayout.LabelField(site.layersInfo[i].layerCount.ToString());
            }
            else
            {
                EditorGUILayout.MinMaxSlider(ref site.layersInfo[i - 1].percentage, ref site.layersInfo[i].percentage, 0, 1, GUILayout.MinWidth(200));

                site.layersInfo[i].layerCount = Mathf.RoundToInt((site.layersInfo[i].percentage - site.layersInfo[i-1].percentage) * site.underheight);

                EditorGUILayout.LabelField(site.layersInfo[i].layerCount.ToString());
            }

            if (site.layersInfo[i].blockData != null)
            {
                layerCount += site.layersInfo[i].layerCount;
            }

            EditorGUILayout.EndHorizontal();
        }

        if (site.layersInfo.Count < (site.underheight))
        {
            if (GUILayout.Button("Add Layer"))
            {
                if (site.layersInfo.Count > 0)
                {
                    float lastPercentage = site.layersInfo[site.layersInfo.Count - 1].percentage;
                    ConstructionSiteGenerator.LayerInfo layer = new ConstructionSiteGenerator.LayerInfo();
                    layer.percentage = lastPercentage;
                    site.layersInfo.Add(layer);
                }
                else
                {
                    site.layersInfo.Add(new ConstructionSiteGenerator.LayerInfo());
                }
            }
        }


        if (layerCount == site.underheight && GUILayout.Button("Generate File"))
        {
            site.CreateFile();
        }
    }
}
