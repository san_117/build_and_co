﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

[System.Serializable]
public class ConstructionSiteData
{
    public int widht = 50, lenght = 50;
    public int height = 50, underheight = 50;

    public LayerData[] layers;

    /// <summary>
    /// Create an new solid constructionSite
    /// </summary>
    /// <param name="widht"></param>
    /// <param name="lenght"></param>
    /// <param name="height"></param>
    /// <param name="underheight"></param>
    public ConstructionSiteData(int widht, int lenght, int height, int underheight)
    {
        this.widht = widht;
        this.lenght = lenght;
        this.height = height;
        this.underheight = underheight;

        layers = new LayerData[height + underheight + 1];

        for (int i = 0; i < height + underheight + 1; i++)
        {
            layers[i] = new LayerData(widht, lenght, i - underheight, "0:0");
        }

    }

    /// <summary>
    /// Create a Construction site data with layers
    /// </summary>
    /// <param name="widht"></param>
    /// <param name="lenght"></param>
    /// <param name="height"></param>
    /// <param name="underheight"></param>
    public ConstructionSiteData(int widht, int lenght, int height, int underheight, LayerData[] layers)
    {
        this.widht = widht;
        this.lenght = lenght;
        this.height = height;
        this.underheight = underheight;
        this.layers = layers;
    }

    [System.Serializable]
    public class LayerData
    {
        public int level;
        public int widht;
        public int height;

        [SerializeField] public List<TileData> tiles = new List<TileData>();

        /// <summary>
        /// Create Layer Fill with an Tile type
        /// </summary>
        /// <param name="widht"></param>
        /// <param name="height"></param>
        /// <param name="level"></param>
        /// <param name="blockID"></param>
        public LayerData(int widht, int height, int level, string blockID)
        {
            this.height = height;
            this.widht = widht;
            this.level = level;

            tiles = new List<TileData>();

            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < widht; x++)
                {
                    tiles.Add(new TileData(new GridCoord(x, level, y), blockID));
                }
            }
        }

        [System.Serializable]
        public class TileData
        {
            public string blockID;
            public GridCoord coord;

            public TileData(GridCoord coord, string blockID)
            {
                this.coord = coord;
                this.blockID = blockID;
            }
        }
    }
}
