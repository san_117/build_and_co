﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameData {

    public string fileName = "new GameData";

    public string playerName;
    public string companyName;

    public ConstructionSiteData[] data;
}
