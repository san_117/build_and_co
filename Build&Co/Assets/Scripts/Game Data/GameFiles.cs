﻿using UnityEngine;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

[CreateAssetMenu()]
public class GameFiles : ScriptableObject
{
    private static GameFiles _singleton;

    public GameData currentData;

    public List<GameData> gameFiles = new List<GameData>();

    private static GameFiles Singleton
    {
        get
        {
            if (_singleton == null)
                _singleton = Resources.Load<GameFiles>("GameFiles/Game Files");

            return _singleton;
        }
    }

    public static void SaveGameData()
    {
        if (!Directory.Exists(Application.persistentDataPath + "/Game Files"))
        {
            Directory.CreateDirectory(Application.persistentDataPath + "/Game Files");
        }

        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Open(Application.persistentDataPath + "/Game Files/" + Singleton.currentData.fileName + ".ra", FileMode.OpenOrCreate);
        bf.Serialize(file, Singleton.currentData);
        file.Close();
    }

    public static void FetchGameData()
    {
        DirectoryInfo info = new DirectoryInfo(Application.persistentDataPath + "/Game Files");
        FileInfo[] fileInfo = info.GetFiles();

        List<GameData> datas = new List<GameData>();

        foreach (FileInfo file in fileInfo)
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream fileStream = File.Open(file.FullName, FileMode.Open);
            GameData serializableSaveData = (GameData)bf.Deserialize(fileStream);
            fileStream.Close();

            serializableSaveData.fileName = file.Name;
            serializableSaveData.fileName = file.Name.Substring(0,file.Name.Length - file.Extension.Length);
            datas.Add(serializableSaveData);
        }

        Singleton.gameFiles = datas;
    }

    public void DeleteGameFile(GameData data)
    {
        DirectoryInfo info = new DirectoryInfo(Application.persistentDataPath + "/Game Files");
        FileInfo[] fileInfo = info.GetFiles();

        foreach (FileInfo file in fileInfo)
        {
            string fileName = file.Name.Substring(0, file.Name.Length - file.Extension.Length);

            if (data.fileName == fileName)
            {
                File.Delete(file.FullName);
            }
        }
    }
}
