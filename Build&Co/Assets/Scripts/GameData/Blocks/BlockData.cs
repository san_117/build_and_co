﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "GameData/Block")]
public class BlockData : ScriptableObject
{
    [Header("Basic Data")]
    public string block_name;
    public string id;

    [Header("Render")]
    public Mesh mesh;
    public Material mat;

    [Header("Info")]
    public bool isTransparent;
    public bool isWalkable;
}
