﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameAssets
{
    private static Dictionary<string, Texture2D> _textures = new Dictionary<string, Texture2D>();
    private static Dictionary<string, BlockData> _blocks = new Dictionary<string, BlockData>();

    public static Texture2D GetTexture(string texture)
    {
        if (_textures.Count == 0)
        {
            Texture2D[] resourcesTextures = Resources.LoadAll<Texture2D>("Game Assets/Textures2D");

            foreach (Texture2D t in resourcesTextures)
            {
                _textures.Add(t.name , t);
            }
        }

        if (_textures.ContainsKey(texture))
        {
            return _textures[texture];
        }
        else
        {
            Debug.Log("Texture '" + texture + "' not exists in game assets!");
            return null;
        }
    }

    public static BlockData GetBlock(string blockID)
    {
        if (_blocks.Count == 0)
        {
            BlockData[] resourcesMeshes = Resources.LoadAll<BlockData>("GameData/Blocks");

            foreach (BlockData b in resourcesMeshes)
            {
                _blocks.Add(b.id, b);
            }
        }

        if (_blocks.ContainsKey(blockID))
        {
            return _blocks[blockID];
        }
        else
        {
            Debug.Log("Block with ID '" + blockID + "' not exists in game assets!");
            return null;
        }
    }
}
